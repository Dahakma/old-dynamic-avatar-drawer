import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {coverNipplesIfHaveNoBreasts} from "../draw/draw";
import {Layer} from "../util/canvas";
import {
	extractPoint,
	drawPoints, 
	splitCurve,
	breakPoint,
	clone,
	adjust,
	simpleQuadratic,
} from "drawpoint/dist-esm";

import {
	straightenCurve,
	findBetween,
	lineLineIntersection,
	getLimbPointsNegative,
	//getLacingPoints,
} from "../util/auxiliary";

import {
	DressBreastPart,
	SuperSleevePart,
	DetachedSleevePart,
	calcDressCleavage,
	LacingPart,
	calcStrapsCleavage,
	calcDualCleavage,
	DualHighlightedBreastPart,
	DualBreastPart,
	DualSleevePart,
} from "./dress";

export function calcTopBody(ex){
	let armpit = clone(ex.armpit);
	let lat = clone(ex.lat);
	//looseness of waist points 
	let hip = adjust(ex.hip, 0, 0);
	let waist = adjust(ex.waist,(this.thickness * 0.8), 0); //last remnant of sweater
	
	//to have top loose around the waist (the same function as for dress)
	{
		let top = armpit;
		if(lat)top = lat;
		let mid = lineLineIntersection(top,hip,{x:0,y:waist.y},{x:100,y:waist.y});
		if(mid.x>waist.x){
			waist.x = findBetween(waist.x, mid.x,this.sideLoose);
			straightenCurve(armpit,waist,this.sideLoose);
			straightenCurve(waist,hip,this.sideLoose);
		};
	}
	
	let out;
	//coverage of waist
	if (this.waistCoverage > 1){
		let sp = splitCurve(1-(this.waistCoverage-1),ex.armpit,waist); //THIS????????
		waist = void 0;
		hip = void 0;
		out = sp.left.p2;
	}else if (this.waistCoverage >= 0){
		let sp = splitCurve(1-this.waistCoverage,waist,hip);
		//waist =  adjust(ex.hip, 0, 0);
		hip = void 0;
		out = sp.left.p2;
    }else{
        let sp = splitCurve(Math.abs(this.waistCoverage),hip,ex.thigh.out);
		out = sp.left.p2;
    }
	
	//bottom
	let bottom = {
			y:out.y-3,
			x:-0.2,
		};
	
	bottom.cp1 = {
		x: bottom.x * 0.5 + out.x * 0.5,
		y: bottom.y
	};
	
	return {
		armpit,
		lat,
		waist,
		hip, 
		out, 
		bottom,
	};
}


//basically calcDressBase but only goes to waist
export function calcTee(ex){
	
	//waist points
	const {armpit,lat,waist,hip,out,bottom} = calcTopBody.call(this, ex);
	
	//cleavage
	const {
		cleavageBot, cleavageTop, neck, collarbone
	} = calcDressCleavage.call(this,ex);
	//checking of bottom limit was removed because DressBreastPart calculate it differently (based on legCoverage)
	//it is assumed the tee won't be used with such deep cleavage for this to matter 
	
	return {
		cleavageBot,
		cleavageTop,
		neck,
		collarbone,
		
		armpit,
		lat,
		waist,
		hip,
		out,
		bottom
	};
}


export class TeePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts		   : ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
        }, {
		}, ...data);
    }

    renderClothingPoints(ex, ctx) {

		const {
			cleavageBot, cleavageTop, neck, collarbone, armpit, waist, hip, out, bottom,
		} = calcTee.call(this, ex);
		
		 Clothes.simpleStrokeFill(ctx, ex, this);

		ctx.beginPath();
		drawPoints(ctx, 
			cleavageBot,  
			cleavageTop,  
			neck, 
			collarbone, 
			armpit,  
			waist,
			hip,
			out,
			bottom
			
		);
		ctx.fill();
		ctx.stroke();

    }
}


//basically Tee + SuperPanties
export class LeotardPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
        }, {
		}, ...data);
    }

    renderClothingPoints(ex, ctx) {
		let temp;
		const {
			cleavageBot, cleavageTop, neck, collarbone, armpit, waist, hip, out, bottom,
		} = calcTee.call(this, ex);
				
		//bottom
		const bot = adjust(clone(ex.groin),-0.2,0);		
		temp = splitCurve(this.genCoverage,ex.groin,extractPoint(ex.thigh.top));	
		const botOut = extractPoint(temp.left.p2);
		
		//bottom curve (hip ? hip : waist)
		temp = splitCurve(0.5, out, botOut );	
		botOut.cp1 = extractPoint(temp.left.p2);
		botOut.cp1.x+=this.curveBotX-9;
		botOut.cp1.y+=this.curveBotY+5;
	
		Clothes.simpleStrokeFill(ctx, ex, this);

		ctx.beginPath();
		drawPoints(ctx, 
			cleavageBot,  
			cleavageTop,  
			neck, 
			collarbone, 
			armpit,  
			waist,
			hip,
			out,
			botOut,
			bot,
			//out,
			//bottom
			
		);
		ctx.fill();
		ctx.stroke();

    }
}


//straps and everything above breasts is in the genital layer which looks good but potentially introduce issues with clipping
export class HalterTopBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);

		//check and recalculate cleavage if deep beyond limit
		function checkCleavage(context,bottom){
			//TODO - come up with some better cleavage checking function
			/*if(cleavage.y <bottom.y){
				cleavage.y = bottom.y+3;
			}
			*/			
			sp = splitCurve(0.5,cleavage,cleavageTopIn);
			cleavageTopIn.cp1 = {
				x: sp.left.p2.x+context.curveCleavageX,
				y: sp.left.p2.y+context.curveCleavageY,
			};
		}
		
		//CLEAVAGE
		let sp = splitCurve(this.cleavageCoverage,ex.neck.cusp, ex.groin);
		let cleavage = {
			x : -0.2,
			y : sp.left.p2.y
		};
		
		const {
			cleavageBot,
			cleavageTopIn,
			neckIn,
			collarboneIn,
			shoulderIn,
			cleavageTopOut,
			neckOut,
			collarboneOut,
			shoulderOut,
		} = calcStrapsCleavage.call(this, ex, this.innerNeckCoverage, this.outerNeckCoverage);
	
		const armpit = extractPoint(ex.armpit);
		
		
		//NO BREASTS
		if (ex.hasOwnProperty("breast") === false) {
			
			let bottom = {
				x: -0.2,
				y: armpit.y-1
			};	
			if(bottom.y > cleavage.y) bottom = void 0; 
			checkCleavage(this,bottom);
			
			ctx.beginPath();
			drawPoints(ctx,
				cleavageBot,
				cleavageTopIn,
				neckIn,
				collarboneIn,
				shoulderIn,
				cleavageTopOut,

				armpit,
				bottom
			);
			ctx.fill();
			
			ctx.beginPath();
			drawPoints(ctx,
				cleavageBot,
				cleavageTopIn,
				neckIn,
				collarboneIn,
				shoulderIn,
				cleavageTopOut,
				armpit,
			);	
			ctx.stroke();
			
			coverNipplesIfHaveNoBreasts(ex, ctx, this)		
			return;
		}
		
		//BREASTS		
		//calculate points - I really dunno how, but it do 
		const breastTip = adjust(ex.breast.tip, 0.1, 0);
		const breastBot = adjust(ex.breast.bot, 0, -0.1);
		
		let bottom = {
			x: -0.2,
			y: breastBot.y
		};
		
		//this enables the cleavage to go very deep
		let bottomOut;
		if(cleavage.y < breastBot.y){
			bottom.y = cleavage.y - 3;
			bottomOut = {
				x: breastBot.x,
				y: bottom.y
			};
			if(cleavage.y < breastBot.y - 10){
				bottomOut.y += 10;
			}
		};
		
		checkCleavage(this,bottom);
		
		ctx.beginPath();
		drawPoints(ctx,
			cleavageBot,
			cleavageTopIn,
			neckIn,
			collarboneIn,
			shoulderIn,
			cleavageTopOut,
	
			breastTip,
			breastBot,
			bottomOut,
			bottom
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx,
			cleavageBot,
			cleavageTopIn,
			neckIn,
			collarboneIn,
			shoulderIn,
			cleavageTopOut,
			breastTip,
			breastBot
		);
		ctx.stroke();
    }
}
 

export class HalterTopChestPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		if(this.waistCoverage>=2) return;
		
		//waist points
		const {armpit,lat,waist,hip,out,bottom} = calcTopBody.call(this, ex);

		const temp = splitCurve(this.cleavageCoverage,ex.neck.cusp, ex.groin);
		const top = {x : -0.1};
		let topToArmpit; 
		
		//the cleavage goes even bellow breasts 
		if(temp.left.p2.y < ex.armpit.y){
			top.y = temp.left.p2.y;
			if(ex.breast){
				topToArmpit = extractPoint(ex.breast.bot);
			}else{
				topToArmpit = void 0;
			}
		}else{
			top.y = ex.armpit.y;
		};
		
		ctx.beginPath();
		drawPoints(ctx, 
			armpit,  
			lat,
			waist,
			hip,
			out,
			bottom,
			top,
			topToArmpit,
			extractPoint(armpit)
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx, 
			armpit, 
			lat,
			waist,
			hip,
			out,
			bottom
		);
		ctx.stroke();
		
    }
}


export class TubeTopBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		if (coverNipplesIfHaveNoBreasts(ex, ctx, this)) {
            return;
        }
		Clothes.simpleStrokeFill(ctx, ex, this);

		let topOut = [];
		//for transformations:
		if(this.chestCoverage>1){
			if(this.chestCoverage>2)this.chestCoverage=1.9;
			let coverage = 1-(this.chestCoverage-1)
			topOut = getLimbPointsNegative(ex.neck.top,ex.breast.top,1-(this.chestCoverage-1),ex.neck.top,ex.neck.cusp,ex.collarbone, extractPoint(ex.breast.top) );		
			topOut[0] = extractPoint(topOut[0]);
			topOut[topOut.length] = adjust(ex.breast.tip, 0.1, 0); 
		//normal:
		}else{
			let temp = splitCurve(1-this.chestCoverage,ex.breast.top,ex.breast.tip);
			topOut[0] = extractPoint(temp.left.p2);
			topOut[1] = adjust(temp.right.p2, 0.1, 0); //tip
			
		}
		
		const topIn = {
			x : -0.2,
			y : topOut[0].y + 1,
		};
		
		topOut[0].cp1 = {
			x: (topOut[0].x+topIn.x) * 0.8,
			y: topIn.y
		};
	
		const bot = adjust(ex.breast.bot, 0, -0.1);
		
		const bottom = {
			x: -0.2,
			y: bot.y
		};
		
		ctx.beginPath();
		drawPoints(ctx,
			topIn,
			...topOut,
			bot,
			bottom
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx,
			topIn,
			...topOut,
			bot
		);
		ctx.stroke();
		
    }
}


export class TubeTopChestPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		if(this.waistCoverage>=2)return;
		
		//waist points
		const {armpit,lat,waist,hip,out,bottom} = calcTopBody.call(this, ex);
		
		const top = {x: -0.2}
		top.y = ex.breast ? ex.breast.in.y : ex.armpit.y;
				
		ctx.beginPath();
		drawPoints(ctx, 
			armpit,  
			lat,
			waist,
			hip,
			out,
			bottom,
			top,
			extractPoint(armpit)
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx, 
			armpit, 
			lat,
			waist,
			hip,
			out,
			bottom
		);
		ctx.stroke();
		
    }
}


export class TopGroinPart extends ClothingPart {
    constructor(...data) {
        super({
           layer              : Layer.FRONT,
           loc                : "torso",
           reflect            : true,
           aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		let temp;
		
		//waist points
		if(this.waistCoverage >= 2)return;
		const {armpit, lat, waist, hip, out, bottom} = calcTopBody.call(this, ex);
		
		//top
		temp = splitCurve(this.cleavageCoverage,ex.neck.cusp, ex.groin);
		const top = {x : -0.1};
		let topToArmpit; 
		
		if(temp.left.p2.y < ex.armpit.y){
			top.y = temp.left.p2.y;
			if(ex.breast){
				topToArmpit = extractPoint(ex.breast.bot);
			}else{
				topToArmpit = void 0;
			}
		}else{
			top.y = ex.armpit.y;
		};
		
		//bottom
		const bot = adjust(clone(ex.groin),-0.2,0);		
		temp = splitCurve(this.genCoverage,ex.groin,extractPoint(ex.thigh.top));	
		const botOut = extractPoint(temp.left.p2);
		
		//bottom curve (hip ? hip : waist)
		temp = splitCurve(0.5, out, botOut );	
		botOut.cp1 = extractPoint(temp.left.p2);
		botOut.cp1.x += this.curveBotX - 9;
		botOut.cp1.y += this.curveBotY + 5;
		
		ctx.beginPath();
		drawPoints(ctx, 
			armpit,  
			lat,
			waist,
			hip,
			out,
			botOut,
			bot,
			top,
			topToArmpit,
			extractPoint(armpit)
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx, 
			armpit, 
			lat,
			waist,
			hip,
			out,
			botOut,
			bot,
		);
		ctx.stroke();
		
    }
}


export class BikiniTopBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		if(this.outerNeckCoverage<this.innerNeckCoverage)this.outerNeckCoverage=this.innerNeckCoverage;
		if(this.innerNeckCoverage>this.outerNeckCoverage)this.innerNeckCoverage=this.outerNeckCoverage;
		
		let cusp = ex.neck.cusp;
		if(ex.trapezius)cusp=ex.trapezius;
		let sp = splitCurve(this.outerNeckCoverage,cusp, ex.collarbone);
		let topOut = sp.left.p2;
				
		if(this.innerNeckCoverage<0){
			sp = splitCurve(1+this.innerNeckCoverage, ex.neck.top, cusp);
		}else{
			sp = splitCurve(this.innerNeckCoverage, cusp, ex.collarbone);
		}
		let topIn = extractPoint(sp.left.p2);
				
		
		//NO BREASTS
		if (ex.hasOwnProperty("breast") === false) {
			let armpit = adjust(ex.armpit,0,0);
			let bottom = {
				x: -0.2,
				y: armpit.y-1
			};

			let cleavage = {
				x : -0.2,
				y : bottom.y + (this.radius*2)
			};
			cleavage.cp1 = {
				x: bottom.x + (this.radius*2),
				y: bottom.y + this.radius
			}
			
			
			sp = splitCurve(0.5,cleavage,topIn);
			topIn.cp1 = {
				x: sp.left.p2.x+this.curveCleavageX,
				y: sp.left.p2.y+this.curveCleavageY,
			};
			
			ctx.beginPath();
			drawPoints(ctx,
				cleavage,
				topIn,
				topOut,
				extractPoint(armpit),
				bottom
			);
			ctx.fill();
			
			ctx.beginPath();
			drawPoints(ctx,
				cleavage,
				topIn,
				topOut,
				extractPoint(armpit)
			);	
			ctx.stroke();
			
			coverNipplesIfHaveNoBreasts(ex, ctx, this)
			
			return;
		}
		
		//BREASTS		
		//calculate points - I really dunno how, but it do 
		const tip = adjust(ex.breast.tip, 0.1, 0);
		const bot = adjust(ex.breast.bot, 0, -0.1);
		
		let bottom = {
			x: -0.2,
			y: bot.y
		};
		
		let cleavage = {
			x : -0.2,
			y : bottom.y + (this.radius*2)
		};
		cleavage.cp1 = {
			x: bottom.x + (this.radius*2),
			y: bottom.y + this.radius
		}
			
		sp = splitCurve(0.5,cleavage,topIn);
		topIn.cp1 = {
				x: sp.left.p2.x+this.curveCleavageX,
				y: sp.left.p2.y+this.curveCleavageY,
		};
			
		ctx.beginPath();
		drawPoints(ctx,
			cleavage,
			topIn,
			topOut,
			//top,
			tip,
			bot,
			bottom,
			cleavage,
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx,
			cleavage,
			topIn,
			topOut,
			//top,
			tip,
			bot,
			bottom,
			cleavage,
		);
		ctx.stroke();
		
    }
}


export class DualTeePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
		}, {

		}, ...data);
    }

    renderClothingPoints(ex, ctx) {

		//arm
		let collarbone = clone(ex.collarbone);
	
		//waist points
		const {armpit,lat,waist,hip,out,bottom} = calcTopBody.call(this, ex);
	
		//cleavage
		const cv  = calcDualCleavage.call(this, ex);        Clothes.simpleStrokeFill(ctx, ex, this);

		ctx.beginPath();
		drawPoints(ctx, 
			cv.cleavageBotOut,
			cv.cleavageTopOutUpward,
			cv.neckOut,
			cv.collarboneOut,
			cv.shoulderOut,
			
			armpit,lat,waist,hip,out,bottom
		);
		ctx.fill();

		ctx.fillStyle = this.highlight;
		ctx.beginPath();
		drawPoints(ctx, 
			cv.cleavageBotIn,
			cv.cleavageTopIn,
			cv.neckIn,
			cv.collarboneIn,
			cv.shoulderIn,
			cv.cleavageTopOut,
			cv.cleavageBotOutDownward,
			
		);
		ctx.fill()
			
		ctx.beginPath();
		drawPoints(ctx, 
			cv.cleavageBotIn,
			cv.cleavageTopIn,
			cv.neckIn,
			cv.collarboneIn,
			cv.shoulderIn,
			cv.cleavageTopOut,
			cv.neckOut,
			cv.collarboneOut,
			cv.shoulderOut,
			
			breakPoint, 
			armpit,
			lat,
			waist,
			hip,
			out,
			bottom
		);
		ctx.stroke();
	
    }
}


/**
 * Base Clothing classes
 */
 
export class Top extends Clothing {
    constructor(...data) {
		super({
			clothingLayer  : Clothes.Layer.MID,
			thickness: 0.6,
		}, ...data);
    }
	
	stroke() {
        return "hsla(335, 800%, 30%, 1)";
    }

    fill() {
        return "hsla(335, 100%, 42%, 1)";
    }
}
	
	
export class Tee extends Top {
    constructor(...data) {
        super({
			cleavageOpeness: 0.3,
			cleavageCoverage: 0.16,
			sideLoose: 0,
			waistCoverage: 0,
			curveCleavageX:0,
			curveCleavageY:0,
			legCoverage: 1, //used only by DressBreastPart but important
        }, ...data);
    }

    get partPrototypes() {
        return [
			{ 
                side: null,  
                Part: LacingPart
            },{
                side: null,
                Part: TeePart
            },{ 
                side: null,  
                Part: DressBreastPart
            },{
                side: Part.LEFT,
                Part: SuperSleevePart
            },{
                side: Part.RIGHT,
                Part: SuperSleevePart
            },
			
        ];
    }
}
	
	
export class Leotard extends Top {
    constructor(...data) {
        super({
			cleavageOpeness: 0.2,
			cleavageCoverage: 0.1,
			waistCoverage: 0.5,
			curveCleavageX: 2,
			curveCleavageY: -2,
			armCoverage: 0.8,
			sideLoose: 0,
			genCoverage: 1,
			curveBotX: 6,
			curveBotY: 6,
		
			legCoverage: 1, //used only by DressBreastPart but important
        }, ...data);
    }

    get partPrototypes() {
        return [
			{
                side: null,
                Part: LeotardPart
            },{ 
                side: null,  
                Part: DressBreastPart
            },{
                side: Part.LEFT,
                Part: SuperSleevePart
            },{
                side: Part.RIGHT,
                Part: SuperSleevePart
            },
			
        ];
    }
}


export class HalterTop extends Top {
    constructor(...data) {
        super({
			cleavageCoverage: 0.3,
			outerNeckCoverage: 0.35,
			innerNeckCoverage: 0.15,
			curveCleavageX: 9,
			curveCleavageY: -9,
			waistCoverage: 0.66,
			sideLoose: 0,
        }, ...data);
    }

    get partPrototypes() {
        return [
			{
                side: null,
                Part: HalterTopBreastPart
            },{ 
                side: null,  
                Part: HalterTopChestPart
           }
        ];
    }
}	


export class TubeTop extends Top {
    constructor(...data) {
        super({
			chestCoverage: 0.3,
			waistCoverage: 0.3,
			sideLoose: 0,
        }, ...data);
    }

    get partPrototypes() {
        return [
			{
                side: null,
                Part: TubeTopBreastPart
            },{ 
                side: null,  
                Part: TubeTopChestPart
            },

        ];
    }
}


export class TubeTopSleeves extends Top {
    constructor(...data) {
        super({
			chestCoverage: 0.3,
			waistCoverage: 0.3,
			sideLoose: 0,
			
			shoulderCoverage: 0,
			armCoverage: 0.5,
			armLoose: 0.5,
        }, ...data);
    }

    get partPrototypes() {
        return [
			{
                side: null,
                Part: TubeTopBreastPart
            },{ 
                side: null,  
                Part: TubeTopChestPart
            },{
                side: Part.LEFT,
                Part: DetachedSleevePart
            },{
                side: Part.RIGHT,
                Part: DetachedSleevePart
            },

        ];
    }
}


export class BikiniTop extends Top {
    constructor(...data) {
        super({
			radius: 3.6,
			outerNeckCoverage: 0.35,
			innerNeckCoverage: 0.03,
			curveCleavageX: 14,
			curveCleavageY: -14,
        }, ...data);
    }

    get partPrototypes() {
        return [
          {
                side: null,
                Part: BikiniTopBreastPart
            }
        ];
    }
}


export class Swimsuit extends Top {
    constructor(...data) {
        super({
			cleavageCoverage: 0.3,
			outerNeckCoverage: 0.35,
			innerNeckCoverage: 0.15,
			curveCleavageX: 9,
			curveCleavageY: -9,
			waistCoverage: 0.2,
			sideLoose: 0,
			genCoverage: 1,
			curveBotX: 6,
			curveBotY: 6,
        }, ...data);
    }

    get partPrototypes() {
        return [
          {
                side: null,
                Part: HalterTopBreastPart
            },{ 
                side: null,  
                Part: TopGroinPart
           }
        ];
    }
}	


export class DualTee extends Top {
    constructor(...data) {
        super({
			sideLoose: 0,
			waistCoverage: 0.5,
			legCoverage: 1, //used only by DressBreastPart but important
			
			armCoverage: 0.3,
			armCoverage2: 0,
			
			cleavageOpeness: 0.1,
			cleavageOpeness2: 0.68,
			cleavageCoverage: 0.05,
			cleavageCoverage2: 0.2,
			
			curveCleavageX: 5,
			curveCleavageY: 0,
			curveCleavageX2: 3,
			curveCleavageY2: -5,
				
			highlight: "hsla(268, 42%, 41%, 1)"
			
        }, ...data);
    }
	
	stroke() {
        return "hsla(268, 42%, 41%, 1)";
    }

    fill() {
        return "hsla(268, 44%, 70%, 1)";
    }

    get partPrototypes() {
        return [
			{ 
                side: null,  
                Part: LacingPart
            },{
                side: null,
                Part: DualTeePart
             },{ 
				side: null,
                Part: DualHighlightedBreastPart
            },{ 
                side: null,  
                Part: DualBreastPart
            },{
                side: Part.LEFT,
                Part: DualSleevePart
            },{
                side: Part.RIGHT,
                Part: DualSleevePart
            },
			
        ];
    }
}
