import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {connectEndPoints, coverNipplesIfHaveNoBreasts} from "../draw/draw";
import {Layer} from "../util/canvas";
import {
    extractPoint,
    drawPoints,
    splitCurve,
    breakPoint,
    clone,
	clamp,
    //none,
    adjust,
    reflect,
    interpolateCurve,
} from "drawpoint/dist-esm";
import {
    getLimbPoints,
    getLimbPointsAbovePoint,
	getLimbPointsBellowPoint,
    straightenCurve,
    findBetween,
    lineLineIntersection,
    lineCubicIntersection,
    pointLineIntersection,
    getLacingPoints,
	getDividedLimbPoints
} from "../util/auxiliary";

export class DressBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"]
        }, {}, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);
		
		//nipples with no breasts
        if (coverNipplesIfHaveNoBreasts(ex, ctx, this)) {
            return;
        }
		
        const {
            cleavageBot, cleavageTop
        } = calcDressBase.call(this, ex);
		const {top, tip, bot, inner, cleavage} = calcBreasts.call(this, ex);
		const topAgain = adjust(connectEndPoints(cleavage, top), 0, 0.2);
		
		
		//line showing conture of breasts (outside the clipped zone)
        ctx.beginPath();
        drawPoints(ctx, top, tip, bot);
        ctx.stroke();
		
		ctx.beginPath();
        drawPoints(ctx, top, tip, bot, inner, cleavage, topAgain);
        ctx.clip();
		
		{
			ctx.beginPath();
			const botIn = adjust(cleavageBot, -this.thickness * 0.38, 0);
			const topIn = adjust(cleavageTop, -this.thickness * 0.38, 0);
			const bot = {x: -this.thickness * 0.38, y: botIn.y - 100};
			const botOut = {x: 50, y: ex.breast.bot.y};
			const topOut = {x: 50, y: ex.breast.top.y};
			drawPoints(ctx, botOut, bot, botIn, topIn, topOut);
			ctx.fill(); 
		}
	
		//line showing conture of breasts (inside the clipped zone)
        ctx.beginPath();
        drawPoints(ctx, top, tip, bot);
        ctx.stroke();
		
		//repair cleavage line
        ctx.beginPath();
        drawPoints(ctx, cleavageBot, cleavageTop);
        ctx.stroke();

    }
}

export class DressBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso", "parts leg", "decorativeParts leg"],
            aboveSameLayerParts: ["groin", "leg"],

        }, {
            cleavageOpeness : 0.3,
            cleavageCoverage: 0.3,
            sideLoose       : 0,
            legCoverage     : 0.4,
            legLoose        : 0,
            curveCleavageX  : 0,
            curveCleavageY  : 0,
            bustle          : false,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const {
            cleavageBot, cleavageTop, neck, collarbone, armpit, waist, hip, outerPoints, bottom,
        } = calcDressBase.call(this, ex);

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx,
            cleavageBot,
            cleavageTop,
            neck,
            collarbone,
            armpit,
            ...outerPoints,
            bottom
        );
        ctx.fill();


        ctx.beginPath();
        drawPoints(ctx,
            cleavageBot,
            cleavageTop,
            neck,
            collarbone,
            breakPoint,
            armpit,
            ...outerPoints,
            bottom
        );
        ctx.stroke();

    }
}


export function calcBreasts(ex) {
	
    const top = adjust(ex.breast.top, 0.5, 0.7);
    const tip = adjust(ex.breast.tip, 0.1, 0);
    const bot = adjust(ex.breast.bot, 0, -0.6);
    const inner = adjust(ex.breast.in, -0.9, -0.5);
    const cleavage = adjust(ex.breast.cleavage, -0.6, 0);

	
        /*
        const top = adjust(ex.breast.top, 0.1, 0.2);
        const tip = adjust(ex.breast.tip, 0.1, 0);
        const bot = adjust(ex.breast.bot, 0, -0.1);
        const inner = adjust(ex.breast.in, -0.4, 0);
        const cleavage = adjust(ex.breast.cleavage, -0.1, 0);
        */
		
    return {
        top,
		tip,
		bot,
		inner,
		cleavage,
    };
}


export function calcDressCleavage(ex, bottom) {
    let cleavageBot; 
    let cleavageTop;
	const has_muscles = ex.trapezius.y === ex.neck.cusp.y ? false : true; //TODO probably not a good way to do this
	let neck = has_muscles ? ex.trapezius : ex.neck.cusp;
	let collarbone = ex.collarbone;
	const shoulder = has_muscles ? ex.deltoids : ex.shoulder;

	//cleavage lowest point
    {
        const sp = splitCurve(this.cleavageCoverage, ex.neck.cusp, ex.groin);
        cleavageBot = {x: -0.2, y: sp.left.p2.y};
    }

    //covering even neck:
    if (this.cleavageOpeness < 0) {
        const sp = splitCurve(1 + this.cleavageOpeness, ex.neck.top, neck);
        cleavageTop = extractPoint(sp.left.p2);
        neck = sp.right.p2;
	//revealing even shoulders:
    } else if (this.cleavageOpeness > 1) { 
        const sp = splitCurve(this.cleavageOpeness - 1, collarbone, shoulder);
        cleavageTop = extractPoint(sp.left.p2);
		collarbone = void 0;
		neck = void 0;
	//normal cases:
    } else {
        const sp = splitCurve(this.cleavageOpeness, neck, ex.collarbone);
        cleavageTop = extractPoint(sp.left.p2);
		collarbone = sp.right.p2;
		neck = void 0;
    }

    //check if cleavage goes bellow the bottom of the garment, fixes it if so
    if (!this.lacing && bottom) {
        if (cleavageBot.y < bottom.y) {
            cleavageBot.y = bottom.y + 3;
        }
    }

    //and calculates the cleavage curve
    let sp = splitCurve(0.5, cleavageBot, cleavageTop); //cleavage curve
    cleavageTop.cp1 = {
        x: sp.left.p2.x + this.curveCleavageX,
        y: sp.left.p2.y + this.curveCleavageY,
    };

    return {
        cleavageBot,
        cleavageTop,
        neck,
		collarbone,
    };
}


export function calcStrapsCleavage(ex, inner, outer, bottom) {
    let cleavageBot; 
    let cleavageTopIn;
	let cleavageTopOut;
	const has_muscles = ex.trapezius.y === ex.neck.cusp.y ? false : true; //TODO probably not a good way to do this
	let neckIn = has_muscles ? ex.trapezius : ex.neck.cusp;
	let collarboneIn = ex.collarbone;
	let shoulderIn = has_muscles ? ex.deltoids : ex.shoulder;
	 
	let neckOut = clone(neckIn);
	let collarboneOut = clone(collarboneIn);
	let shoulderOut = clone(shoulderIn);
	
	if(outer < inner ){
		outer = inner;
	}
	if(inner > outer){
		inner = outer;
	}
		
	//cleavage lowest point
    {
        const sp = splitCurve(this.cleavageCoverage, ex.neck.cusp, ex.groin);
        cleavageBot = {x: -0.2, y: sp.left.p2.y};
    }

	//TODO - I'm just not able to figure out the correct simple formula
	function secondCut(fraction, newStart, originalStart, end){
		let sp = splitCurve(fraction, originalStart, end);
		let temp = interpolateCurve(newStart, end, {x:null,y: sp.left.p2.y});
		sp = splitCurve(temp[0].t, newStart, end);	
		return sp;
	}
	
	
    //covering even neck:
    if (inner < 0) {
        const sp = splitCurve(1 + inner, ex.neck.top, neckIn);
        cleavageTopIn = sp.left.p2;
		neckIn = sp.right.p2;
		
		if (outer < 0) {
			//const sp = splitCurve(1 + (outer - inner) /*TODO*/, cleavageTopIn, neckIn);
			//const sp = splitCurve(1 + outer, ex.neck.top, neckIn); TODO
			const sp = secondCut(1+ outer, cleavageTopIn, ex.neck.top, neckIn); //TODO
			
			cleavageTopOut = sp.left.p2;
			neckOut = sp.right.p2;
			neckIn = void 0;
			collarboneIn = void 0;
			shoulderIn = void 0;
			
		} else if (outer > 1) { 
			neckOut = void 0; 
			collarboneOut = void 0; 
			const sp = splitCurve(outer - 1, collarboneIn, shoulderIn);
			cleavageTopOut = sp.left.p2;
			shoulderOut = sp.right.p2;
			shoulderIn = void 0;
			
		} else {
			neckOut = void 0;
			const sp = splitCurve(outer, neckIn, collarboneIn);
			cleavageTopOut = sp.left.p2;
			collarboneIn = void 0;
			collarboneOut = sp.right.p2;
			shoulderIn = void 0;
		}

		
	//revealing even shoulders:
    } else if (inner > 1) { 
        const sp = splitCurve(inner - 1, collarboneIn, shoulderIn);
        cleavageTopIn = sp.left.p2;
		shoulderOut = sp.right.p2;
		
		collarboneIn = void 0;
		neckIn = void 0;
		shoulderIn = void 0;
		
		{
			//TODO
			//const sp = splitCurve((outer - 1) - (inner - 1), cleavageTopIn, shoulderOut);
			/*
			const sp = splitCurve(outer - 1, collarboneOut, shoulderOut);
			cleavageTopOut = sp.left.p2;
			shoulderOut = sp.right.p2;
			*/			
			const sp = secondCut(outer - 1, cleavageTopIn, collarboneOut, shoulderOut); //TODO
			cleavageTopOut = sp.left.p2;
			shoulderOut = sp.right.p2;
			
			collarboneOut = void 0;
			neckOut = void 0;
		}
	//normal cases:
    } else {
        const sp = splitCurve(inner, neckIn, ex.collarbone);
        cleavageTopIn = sp.left.p2;
		collarboneIn = sp.right.p2;
		neckIn = void 0;
				
		if (outer > 1) { 
			collarboneOut = void 0; 
			const sp = splitCurve(outer - 1, collarboneIn, shoulderOut);
			cleavageTopOut = sp.left.p2;
			shoulderOut = sp.right.p2;
			shoulderIn = void 0;
			neckOut = void 0;
			
		} else {
			//TODO - Im just not able to figure out the correct formula
			//const sp = splitCurve((outer - inner) * (1/inner), cleavageTopIn, collarboneIn);
			//const sp = splitCurve(outer, neckOut, collarboneOut);
			const sp = secondCut(outer, cleavageTopIn, neckOut, collarboneOut); //TODO
			cleavageTopOut = sp.left.p2;
			collarboneOut = sp.right.p2;
			
			collarboneIn = void 0;
			neckOut = void 0;
			shoulderIn = void 0;
		}
		
    }

	cleavageTopIn = extractPoint(cleavageTopIn);
	
    return {
        	cleavageBot,
			cleavageTopIn,
			neckIn,
			collarboneIn,
			shoulderIn,
			cleavageTopOut,
			neckOut,
			collarboneOut,
			shoulderOut,

    };
}

export function calcDressBase(ex) {
    //waist
    let armpit = extractPoint(ex.armpit);
    let lat = clone(ex.lat);
    let hip = adjust(ex.hip, 0, 0);
    let waist = adjust(ex.waist, 0, 0); //adjust(ex.waist,(this.thickness * 0.8), 0); //last remnant of sweater

    //to have dress loose around the waist
    {
        const top = lat ? lat : armpit;
        let mid = lineLineIntersection(top, hip, {x: 0, y: waist.y}, {x: 100, y: waist.y});
        if (mid.x > waist.x) {
            waist.x = findBetween(waist.x, mid.x, this.sideLoose);
            straightenCurve(armpit, waist, this.sideLoose);
            straightenCurve(waist, hip, this.sideLoose);
        }/*else{
			hip = extractPoint(hip);
		}*/
    }
    //if(waist.x<armpit.x)waist.x = findBetween(waist.x, findBetween(armpit.x,hip.x,0.5),this.sideLoose);


    //lower waist and legs
    let outerPoints;
    if (this.legCoverage < 0) {
        outerPoints = getLimbPoints(armpit, hip, 1 + this.legCoverage, armpit, lat, waist, hip);
    } else if (this.legCoverage == 0) {
        outerPoints = [armpit, lat, waist, hip];
    } else {
        let waistPoints = [armpit, lat, waist];
        let legPoints;

        if (this.legLoose > 0) {
            legPoints = getLimbPoints(hip, ex.ankle.out, this.legCoverage, hip, ex.thigh.out);
            let totalLegLength = hip.y - ((hip.y - ex.ankle.out.y) * this.legCoverage);
            if (totalLegLength < ex.groin.y) {
                legPoints = [];
                legPoints[legPoints.length] = clone(hip);
                legPoints[legPoints.length] = clone(ex.thigh.out);

                if (this.bustle) {
                    legPoints[legPoints.length] = extractPoint(ex.thigh.out);
                }

                legPoints[legPoints.length - 1].y = totalLegLength;
                legPoints[legPoints.length - 1].x += this.legLoose * 30 * this.legCoverage;

            }
        } else {
            legPoints = getLimbPoints(hip, ex.ankle.out, this.legCoverage, hip, ex.thigh.out, ex.knee.out, ex.calf.out,
                ex.ankle.out);
        }

        outerPoints = waistPoints.concat(legPoints);
    }

    //bottom
    let bottom = {
        y: outerPoints[outerPoints.length - 1].y,
        x: -0.1,
    };

    //cleavage
    const {
        cleavageBot, cleavageTop, neck, collarbone
    } = calcDressCleavage.call(this, ex, bottom);

    //bottom curve
    outerPoints[outerPoints.length - 1].y += 2;
    bottom.y += -2;
    bottom.cp1 = {
        x: bottom.x * 0.5 + outerPoints[outerPoints.length - 1].x * 0.5,
        y: bottom.y
    };

    return {
        cleavageBot,
		cleavageTop,
		neck,
		collarbone,
		armpit,
		waist,
		hip,
		outerPoints,
		bottom,
    };


}


export function calcDualCleavage(ex){
	if(this.cleavageOpeness > this.cleavageOpeness2) this.cleavageOpeness2 = this.cleavageOpeness;
	if(this.cleavageCoverage > this.cleavageCoverage2) this.cleavageCoverage2 = this.cleavageCoverage;
 
	const sp = splitCurve(this.cleavageCoverage2, ex.neck.cusp, ex.groin);
	let cleavageBotOut = {x: -0.2, y: sp.left.p2.y};
	
	let {
		cleavageBot,
		cleavageTopIn,
		neckIn,
		collarboneIn,
		shoulderIn,
		cleavageTopOut,
		neckOut,
		collarboneOut,
		shoulderOut,
	} = calcStrapsCleavage.call(this, ex, this.cleavageOpeness, this.cleavageOpeness2);

	let cleavageTopOutUpward = extractPoint(cleavageTopOut);
	let cleavageBotIn = cleavageBot;
	let cleavageBotOutDownward = extractPoint(cleavageBotOut);
	
	//calculates the cleavage curve
	{
		let sp = splitCurve(0.5, cleavageBotIn, cleavageTopIn); //cleavage curve
		cleavageTopIn.cp1 = {
			x: sp.left.p2.x + this.curveCleavageX,
			y: sp.left.p2.y + this.curveCleavageY,
		};
	}
	{
		let sp = splitCurve(0.5, cleavageBotOut, cleavageTopOutUpward); //cleavage curve
		cleavageTopOutUpward.cp1 = {
			x: sp.left.p2.x + this.curveCleavageX2,
			y: sp.left.p2.y + this.curveCleavageY2,
		};
	}
	
	cleavageBotOutDownward.cp1 = cleavageTopOutUpward.cp1;
	
	return {
        	cleavageBotIn, //in in the direction of the cleavage 
			cleavageBotOut,
			cleavageBotOutDownward,
			cleavageTopIn,
			neckIn,
			collarboneIn,
			shoulderIn,
			cleavageTopOut,
			cleavageTopOutUpward,
			neckOut,
			collarboneOut,
			shoulderOut,

    };
}


export class DetachedSleevePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            shoulderCoverage: -0.1,

            armCoverage: 0.5,
            armLoose   : 0,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        if (this.armCoverage <= 0) {
            return;
        }

        const {
            outerArmPoints,
            innerArmPoints,
			connectArmPoints
        } = calcSuperSleeve.call(this, ex);

        Clothes.simpleStrokeFill(ctx, ex, this);

        if (this.shoulderCoverage >= 1) {
            ctx.beginPath();
            drawPoints(ctx,
                ...outerArmPoints,
                ...innerArmPoints,
                connectEndPoints(...connectArmPoints)
            );
            ctx.fill();

            ctx.beginPath();
            drawPoints(ctx,
                ...outerArmPoints,
                ...innerArmPoints
            );
            ctx.stroke();
        } else {
            if (this.shoulderCoverage > 0) {
                let temp = splitCurve(1 - this.shoulderCoverage, ex.collarbone, ex.shoulder);

                //DELTOIDS!!!!
                if (ex.deltoids && ex.collarbone.y != ex.deltoids.y) {
                    temp = splitCurve(1 - this.shoulderCoverage, ex.collarbone, ex.deltoids);
                }
                ;

                outerArmPoints[0] = extractPoint(temp.left.p2);
                outerArmPoints[1] = temp.right.p2;

            } else {
                let temp = splitCurve(Math.abs(this.shoulderCoverage), ex.shoulder, ex.elbow.out);
                outerArmPoints.splice(0, 1);
                if (ex.deltoids && ex.collarbone.y != ex.deltoids.y) {
                    outerArmPoints.splice(0, 1);
                }
                outerArmPoints[0] = extractPoint(temp.left.p2);
            }

            ctx.beginPath();
            drawPoints(ctx,
                ...outerArmPoints,
                ...innerArmPoints,
                outerArmPoints[0]
            );
            ctx.fill();
            ctx.stroke();
        }
    }
}


export class SuperSleevePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            armCoverage: 0.5,
            armLoose   : 0,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        if (this.armCoverage <= 0) {
            return;
        }

        const {
            outerArmPoints,
            innerArmPoints,
			connectArmPoints
        } = calcSuperSleeve.call(this, ex);

		Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
            ...outerArmPoints,
            ...innerArmPoints,
            connectEndPoints(...connectArmPoints)
        );
        ctx.fill();


        ctx.beginPath();
        drawPoints(ctx,
            ...outerArmPoints,
            ...innerArmPoints
        );
        ctx.stroke();
    }
}

export function calcSuperSleeve(ex) {
    let outerArmPoints = [];
    let innerArmPoints = [];

    const highPoint = ex.collarbone;
    const lowPoint = (ex.hand)? ex.hand.palm : ex.elbow.out;

    //loose //
    if (this.armLoose > 0) {
        const limbPoints = [ex.collarbone];
        // in case we don't have hands
        let innerEndPoint = adjust(ex.elbow.out, 0, 0);
        if (ex.hand) {
            innerEndPoint = adjust(ex.wrist.in, 0, 0);
            innerEndPoint.y = ex.hand.tip.y;
        }

        let shoulder = adjust(ex.shoulder, 0.75 * (5 * this.armLoose), 0);
        let deltoid; //DELTOID!!!
        if (ex.deltoids && ex.deltoids.y != ex.collarbone.y) {
            deltoid = adjust(ex.deltoids, 1 * (5 * this.armLoose), 0);
        }
        limbPoints.push(deltoid, shoulder);

        if (ex.hand) {
            let knuckle = adjust(ex.hand.knuckle, 1.5 * (5 * this.armLoose), 0);
            let tip = adjust(ex.hand.tip, 1 * (5 * this.armLoose), 0);
            limbPoints.push(knuckle, tip);
        } else {
            limbPoints.push(ex.elbow.out, ex.elbow.in);
        }

        outerArmPoints = getLimbPoints(highPoint, lowPoint, this.armCoverage, ...limbPoints);

        innerArmPoints = getLimbPointsAbovePoint(outerArmPoints[outerArmPoints.length - 1], true, ex.armpit,
            innerEndPoint);
        if (typeof innerArmPoints[0] !== "undefined") {
            innerArmPoints[0].x -= 1 * (5 * this.armLoose)
        }
        if (typeof innerArmPoints[1] !== "undefined") {
            innerArmPoints[1].x -= 1 * (5 * this.armLoose)
        }
        //fitting
    } else {
        const deltoid = (ex.deltoids && ex.deltoids.y != ex.collarbone.y) ? ex.deltoids : void 0;
        const limbPoints = [ex.collarbone, deltoid, ex.shoulder, ex.elbow.out];
        if (ex.hand) {
            limbPoints.push(ex.wrist.out, ex.hand.knuckle, ex.hand.fist, ex.hand.tip);
        }
        outerArmPoints = getLimbPoints(highPoint, lowPoint, this.armCoverage, ...limbPoints);
        //this will turn the sleeve into glove
        if (ex.hand && outerArmPoints[outerArmPoints.length - 1].y <= ex.thumb.tip.y) {
            outerArmPoints[outerArmPoints.length] = ex.hand.palm;
            innerArmPoints = [ex.armpit, ex.elbow.in, ex.wrist.in, ex.thumb.out, ex.thumb.tip];
            innerArmPoints.reverse();
        } else {
            const abovePoints = [ex.armpit, ex.elbow.in];
            if (ex.hand) {
                abovePoints.push(ex.wrist.in, ex.thumb.out, ex.thumb.tip);
            }
            innerArmPoints = getLimbPointsAbovePoint(outerArmPoints[outerArmPoints.length - 1], true, ...abovePoints);
        }
    }

    if (this.armCoverage < 1) { //to have the bottom of the sleeve straight, not desirable if glove
        innerArmPoints[0] = extractPoint(innerArmPoints[0]);
    }

	//connection to the body
	const connectArmPoints = [adjust(ex.armpit,-0.5,0), adjust(ex.collarbone,-0.5,0)];
	
    return {
        outerArmPoints,
        innerArmPoints,
		connectArmPoints,
    };
}


export class LacingPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : false,
            aboveParts: ["parts chest", "decorativeParts chest"]

        }, {
            lacing : false,
            crosses: 0
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        if (!this.lacing) {
            return;
        }

        Clothes.simpleStrokeFill(ctx, ex, this);

        const {
            cleavageBot, cleavageTop
        } = calcDressCleavage.call(this, ex);

        let crosses = this.crosses; //crosses <0 = automatic number
        if (crosses <= 0) {
            crosses = this.cleavageCoverage * 13;
        }

        let top = reflect(cleavageTop);
        top.cp1 = reflect(cleavageTop.cp1);
        top.cp2 = reflect(cleavageTop.cp2);

        let points = getLacingPoints(cleavageBot, cleavageTop, reflect(cleavageBot), top, crosses, 0);

        ctx.lineWidth = 1;
        ctx.beginPath();
        drawPoints(ctx, ...points.inner, breakPoint, ...points.outer);
        ctx.stroke();

    }
}

/* TODO
export class SleeveHemPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
			hemArmHeight: 5,
			hemArmWidth: 5,
			hemColor: "",
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		if(this.hemArmHeight==0||this.hemArmWidth==0)return
		Clothes.simpleStrokeFill(ctx, ex, this);
        if(this.hemColor){
			ctx.fillStyle=this.hemColor
		}else{
			ctx.fillStyle=ctx.strokeStyle
		};
		
		const {
           outerArmPoints,
		   innerArmPoints
        } = calcSuperSleeve.call(this, ex);
		
		
		//var temp = extractPoint(outerArmPoints[outerArmPoints.length-1]);
		//hemOut = adjust(temp,5,-5) ;
		
		var hemOut = adjust(extractPoint(outerArmPoints[outerArmPoints.length-1]),this.hemArmWidth,-this.hemArmHeight);
		var	hemIn = adjust(extractPoint(innerArmPoints[0]),-this.hemArmWidth,-this.hemArmHeight) ;
		
		 //bottom curve
		hemIn.cp1 = {
			x: 0.5*(hemOut.x+hemIn.x),
			y: hemIn.y-1
		};
	
		ctx.beginPath();
		drawPoints(ctx, 
			outerArmPoints[outerArmPoints.length-1],
			hemOut,
			hemIn,
			extractPoint(innerArmPoints[0])
		);
		ctx.stroke();
		ctx.fill();
	}
}
*/

export class DualBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		//nipples with no breasts
        if (coverNipplesIfHaveNoBreasts(ex, ctx, this)) {
            return;
        }
		
		const cv  = calcDualCleavage.call(this, ex);
		const {top, tip, bot, inner, cleavage} = calcBreasts.call(this, ex);
		const topAgain = adjust(connectEndPoints(cleavage, top), 0, 0.2);

		ctx.beginPath();
        drawPoints(ctx, top, tip, bot, inner, cleavage, topAgain);
        ctx.clip();
		
		{
			ctx.beginPath();
			const botIn = adjust(cv.cleavageBotOut, -this.thickness * 0.38, 0);
			const topIn = adjust(cv.cleavageTopOutUpward, -this.thickness * 0.38, 0);
			const bot = {x: -this.thickness * 0.38, y: botIn.y - 100};
			const botOut = {x: 50, y: ex.breast.bot.y};
			const topOut = {x: 50, y: ex.breast.top.y};
			drawPoints(ctx, botOut, bot, botIn, topIn, topOut);
			ctx.fill(); 
		}
		
		
    }
}


export class DualHighlightedBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {}, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		//nipples with no breasts
        if (coverNipplesIfHaveNoBreasts(ex, ctx, this)) {
            return;
        }
		
		const cv  = calcDualCleavage.call(this, ex);
		const {top, tip, bot, inner, cleavage} = calcBreasts.call(this, ex);
		const topAgain = adjust(connectEndPoints(cleavage, top), 0, 0.2);
 
		//inner part
		ctx.fillStyle = this.highlight;
		
		ctx.beginPath();
		drawPoints(ctx, top, tip, bot, inner, cleavage, topAgain);
		ctx.clip();
		
		{
			ctx.beginPath();
			drawPoints(ctx,
				adjust(cv.cleavageBotIn,-this.thickness*0.38,0),
				adjust(cv.cleavageTopIn,-this.thickness*0.38,0),
				adjust(cv.cleavageTopOut,-this.thickness*0.38,0),
				adjust(cv.cleavageBotOutDownward,-this.thickness*0.38,0),
			);
			ctx.fill(); 
		}
		
		//repair cleavage line
		ctx.beginPath();
        drawPoints(ctx, cv.cleavageBotIn, cv.cleavageTopIn,);
        ctx.stroke();
		
		//line showing conture of breasts (inside the clipped zone)
        ctx.beginPath();
        drawPoints(ctx, top, tip, bot);
        ctx.stroke();

    }
}


export class DualDressBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg","decorativeParts leg"], 
	//		aboveSameLayerParts: ["groin", "leg"], 
			
        }, {
            cleavageOpeness: 0.2,
			cleavageCoverage: 0.2,
			sideLoose: 0,
			legCoverage: 0.9,
			legCoverage2: 0.4,
			legLoose: 0,
			curveCleavageX:0,
			curveCleavageY:0,
			bustle: false,
			
			cleavageOpeness2: 0.4,
			cleavageCoverage2: 0.4,
			curveCleavageX2:0,
			curveCleavageY2:0,
			highlight: "hsla("+100+","+50+"%,"+50+"%,"+0.5+")",
			
			legAdjust: 0,
			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		
		let {
			cleavageBot,cleavageTop,neck,collarbone,armpit,waist,hip,outerPoints,bottom,
		} = calcDressBase.call(this, ex);
 			
		const cv  = calcDualCleavage.call(this, ex);
		
		//stopping the cleavage from being deeper than the dress
		if (!this.lacing && bottom) {
			if (cv.cleavageBotOut.y < bottom.y) {
				cv.cleavageBotOut.y = bottom.y + 3;
			}
			if (cv.cleavageBotIn.y < bottom.y) {
				cv.cleavageBotIn.y = bottom.y + 5;
			}
		};
	
		//dual skirt
		const highPoint = ex.hip;
		const lowPoint = ex.ankle.out;
		if(this.legCoverage2 > this.legCoverage) this.legCoverage2 = this.legCoverage;
		const y_divider =  highPoint.y - ( (highPoint.y-lowPoint.y) * this.legCoverage2 );
		const legAdjust = /* ? -this.legAdjust :*/ this.legAdjust; //TODO 
		outerPoints = getDividedLimbPoints(y_divider + legAdjust, outerPoints, false);
		const middleConnection = adjust(
			bottom,
			0,
			(outerPoints.bellow[0]) ? (outerPoints.middle.y - outerPoints.bellow[outerPoints.bellow.length - 1].y + legAdjust) : 0
		)
		
		
        Clothes.simpleStrokeFill(ctx, ex, this);
		
		//base part 
		ctx.beginPath();
		drawPoints(ctx, 			
			cv.cleavageBotOut,
			cv.cleavageTopOutUpward,
			cv.neckOut,
			cv.collarboneOut,
			//shoulderOut,
			armpit,  
			...outerPoints.above, 
			outerPoints.middle,
			middleConnection,
			cv.cleavageBotOut,
		);
		ctx.fill();
		
		ctx.fillStyle = this.highlight;
		
		//skirt part
		if(outerPoints.bellow[0]){
			const toOuterPointsMiddle = extractPoint(outerPoints.middle);
			toOuterPointsMiddle.cp1 = {
				x: toOuterPointsMiddle.x * 0.5 + middleConnection.x * 0.5,
				y: middleConnection.y
			};
			
			ctx.beginPath();
			drawPoints(ctx, 
				outerPoints.middle,
				...outerPoints.bellow, 
				bottom,
				extractPoint(middleConnection),
				toOuterPointsMiddle
			);
			ctx.fill();
		}
		
		//cleavage part
		ctx.beginPath();
		drawPoints(ctx, 
			cv.cleavageBotIn,
			cv.cleavageTopIn,
			cv.neckIn,
			cv.collarboneIn,
			//shoulderIn,
			cv.cleavageTopOut,
			cv.cleavageBotOutDownward,
		);
		ctx.fill()

		//stroke
		ctx.beginPath();
		drawPoints(ctx, 
			cv.cleavageBotIn,
			cv.cleavageTopIn,
			cv.neckIn,
			cv.collarboneIn,
			//shoulderIn,
			cv.cleavageTopOut,
			cv.neckOut,
			cv.collarboneOut,
			//shoulderOut,

			breakPoint, 
			armpit,   
			...outerPoints.above,
			outerPoints.middle,
			...outerPoints.bellow,
			bottom
		);
		ctx.stroke();

    }
}


export class DualSleevePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            armCoverage: 0.9,
			armCoverage2: 0.2,
			armAdjust: 1,
			armLoose: 0,
			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		if(this.armCoverage<=0) return;
		
		const highPoint = ex.collarbone;
		const lowPoint = (ex.hand)? ex.hand.palm : ex.elbow.out;
		this.armCoverage2 = clamp(this.armCoverage2, 0, this.armCoverage)
		
		const y_divider =  highPoint.y - ( (highPoint.y-lowPoint.y) * this.armCoverage2 );
		
		const points = calcSuperSleeve.call(this, ex);
		
		let temp;
		temp = getDividedLimbPoints(y_divider - this.armAdjust, points.outerArmPoints, false);
		
		const shorterOuter = temp.above;
		const middleOuter = temp.middle;
		const longerOuter = temp.bellow;
		
		temp = getDividedLimbPoints(y_divider + this.armAdjust, points.innerArmPoints.reverse(), true);
		
		const shorterInner = temp.above;
		let middleInner = temp.middle;
		const longerInner = temp.bellow;
		
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		//shorter
		ctx.beginPath();
		drawPoints(ctx, 
			...shorterOuter,
			middleOuter,
			middleInner,
			...shorterInner,
			extractPoint(ex.armpit),
			connectEndPoints(...points.connectArmPoints)
		);
		ctx.fill();
		
		//longer
		ctx.fillStyle = this.highlight;
		ctx.beginPath();
		drawPoints(ctx, 
			middleOuter,
			...longerOuter, 
			...longerInner,
			middleInner,
			(this.armCoverage2 <= 0 ? connectEndPoints(...points.connectArmPoints) : void 0),
		);
		ctx.fill();
		
		//stroke
		ctx.beginPath();
		drawPoints(ctx, 
			...shorterOuter,
			middleOuter,
			...longerOuter, 
			...longerInner,
			middleInner,
			...shorterInner,
		);
		ctx.stroke();

    }
}


export class BandDressBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso", "parts leg", "decorativeParts leg"],
            aboveSameLayerParts: ["groin", "leg"],

        }, {
            cleavageOpeness : 0.3,
            cleavageCoverage: 0.3,
            sideLoose       : 0,
            legCoverage     : 0.52,
            legLoose        : 0,
            curveCleavageX  : 0,
            curveCleavageY  : 0,
            bustle          : false,
			
			midUpperCoverage: -0.6,
			midLowerCoverage: 0.00,
			highlight: "hsla(335.2,74.1%,62.2%,0.6)",
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
        const {
            cleavageBot, cleavageTop, neck, collarbone, armpit, waist, hip, outerPoints, bottom,
        } = calcDressBase.call(this, ex);

		let temp;
		let yDivider;
		
		this.midUpperCoverage = clamp(this.midUpperCoverage, -1, this.legCoverage)
		this.midLowerCoverage = clamp(this.midLowerCoverage, this.midUpperCoverage, this.legCoverage)
		
		if(this.midUpperCoverage === this.midLowerCoverage){
			ctx.beginPath();
			drawPoints(ctx,
				cleavageBot,
				cleavageTop,
				neck,
				collarbone,
				armpit,
				...outerPoints,
				bottom
			);
			ctx.fill();


			ctx.beginPath();
			drawPoints(ctx,
				cleavageBot,
				cleavageTop,
				neck,
				collarbone,
				breakPoint,
				armpit,
				...outerPoints,
				bottom
			);
			ctx.stroke();
			
			return;
			
		}
		
		/*
			                         ...upperPoints
			upperIn - (toUpperOut) - upperOut
			                         ...midPoints
			lowerIn - (toLowerOut) - lowerOut
			                         ... lowerPoits
			bottom -                 bottomOut
			
		*/
		
		const bottomOut = outerPoints[outerPoints.length - 1];
		
		if(this.midLowerCoverage >= 0){
			yDivider =  ex.hip.y - ( (ex.hip.y - ex.ankle.out.y) * this.midLowerCoverage );
		}else if(this.midLowerCoverage < 0){
			yDivider =  ex.armpit.y - ( (ex.armpit.y - ex.hip.y) * (1 + this.midLowerCoverage) );	
		};
		
		temp = getDividedLimbPoints(yDivider, [armpit, ...outerPoints, bottom], false);
		
		const lowerPoints = temp.bellow;
		const lowerOut = temp.middle;
		const lowerIn = adjust(bottom, 0, lowerOut.y - bottomOut.y);
		const toLowerOut = extractPoint(lowerOut); //to draw curve to lowerOut from lowerIn
		toLowerOut.cp1 = lowerIn.cp1;
		
		
		if(this.midUpperCoverage >= 0){
			yDivider =  ex.hip.y - ( (ex.hip.y - ex.ankle.out.y) * this.midUpperCoverage );
		}else if(this.midUpperCoverage < 0){
			yDivider =  ex.armpit.y - ( (ex.armpit.y - ex.hip.y) * (1 + this.midUpperCoverage) );	
		};
		
		temp = getDividedLimbPoints(yDivider, [...temp.above, temp.middle], false);
		
		const midPoints = temp.bellow;
		const upperPoints = temp.above;
		const upperOut = temp.middle;
		const upperIn = adjust(bottom, 0, upperOut.y - bottomOut.y);
		const toUpperOut = extractPoint(upperOut);
		toUpperOut.cp1 = upperIn.cp1;
		
		
		//upper part 
		ctx.beginPath();
        drawPoints(ctx,
			cleavageBot,
            cleavageTop,
            neck,
            collarbone,
			...upperPoints,
			upperOut,
			upperIn,
		);
        ctx.fill();
	
		//bottom part
		ctx.beginPath();
        drawPoints(ctx,
			lowerIn,
			toLowerOut,
			...lowerPoints, //TODO skirt - CPs of the first one (TODO - or could be caused by something else)
			bottomOut,
			bottom,
		);
        ctx.fill();
		
		//middle part
		ctx.fillStyle = this.highlight,
		ctx.beginPath();
        drawPoints(ctx,
			upperIn,
			toUpperOut,
			...midPoints,
			lowerOut,
			lowerIn
        );
        ctx.fill();

		//stroke
		ctx.beginPath();
        drawPoints(ctx,
            cleavageBot,
            cleavageTop,
            neck,
            collarbone,
            breakPoint,
            armpit,
            ...outerPoints,
            bottom
        );
        ctx.stroke();
		
    }
}


/**
 * Base Clothing classes
 */
export class Dress extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.MID,
            armCoverage  : 0.5,
            armLoose     : 0,
            thickness    : 1,
        }, ...data);
    }
}


export class SuperDress extends Dress {
    constructor(...data) {
        super({}, ...data);
    }

    stroke() {
        return "hsla(335, 80%, 30%, 1)";
    }

    fill() {
        return "hsla(335, 100%, 42%, 1)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: LacingPart
            }, {
                side: null,
                Part: DressBasePart
            },
            {
                side: null,
                Part: DressBreastPart
            },

            {
                side: Part.LEFT,
                Part: SuperSleevePart
            },
            {
                side: Part.RIGHT,
                Part: SuperSleevePart
            },
        ];
    }
}


export class DualDress extends Dress {
    constructor(...data) {
        super({
			clothingLayer: Clothes.Layer.MID,
            armCoverage  : 0.95,
            armLoose     : 0,
			armAdjust: -2.5,
			
			legCoverage: 0.4,
            legCoverage2: 0,
			
			cleavageCoverage2: 0.29,
			cleavageOpeness2: 0.68,
			curveCleavageX: 6,
			curveCleavageX2: 6,

			highlight: "hsla(0, 50%, 10%, 1)"
        }, ...data);
    }

    stroke() {
        return "hsla(210, 90%, 40%, 1)";
    }

    fill() {
        return "hsla(210, 100%, 50%, 1)";
    }

    get partPrototypes() {
        return [
			{ 
                side: null,  
                Part: LacingPart
            },{
                side: null,
                Part: DualDressBasePart
            },{ 
				side: null,
                Part: DualHighlightedBreastPart
            },{ 
                side: null,  
                Part: DualBreastPart
            },{
				
                side: Part.LEFT,
                Part: DualSleevePart
            },{
                side: Part.RIGHT,
                Part: DualSleevePart
            },
        ];
    }
}


export class BandDress extends Dress {
    constructor(...data) {
        super({}, ...data);
    }

    stroke() {
        return "hsla(335, 80%, 30%, 1)";
    }

    fill() {
        return "hsla(335, 100%, 42%, 1)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: LacingPart
            }, {
                side: null,
                Part: BandDressBasePart
            },
            {
                side: null,
                Part: DressBreastPart
            },

            {
                side: Part.LEFT,
                Part: SuperSleevePart
            },
            {
                side: Part.RIGHT,
                Part: SuperSleevePart
            },
        ];
    }
}