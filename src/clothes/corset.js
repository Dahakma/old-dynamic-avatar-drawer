import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {coverNipplesIfHaveNoBreasts} from "../draw/draw";
import {Layer} from "../util/canvas";
import {
	breakPoint,
    extractPoint,
	drawPoints, 
	splitCurve,
	simpleQuadratic,
	clone,
	adjust,
	clamp,
} from "drawpoint/dist-esm";

import {
	getLimbPointsBellowPoint,
	getLacingPoints,
} from "../util/auxiliary";

import {Top} from "./tops";
import {calcBra} from "./underwear";

export class CorsetBreastPart extends ClothingPart {
	constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }
	
	renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		if (coverNipplesIfHaveNoBreasts(ex, ctx, this)) {
            return;
        }
		
		const bra = calcBra(ex);
      
		bra.out.cp1 = simpleQuadratic(bra.top, bra.out, 0.4, 1);
		bra.top.cp1 = simpleQuadratic(ex.breast.cleavage, bra.top, 0.6, 2);
		
		//based on the dress to cover the dress:
		bra.bot = adjust(ex.breast.bot, 0, -0.6);
		bra.inner = adjust(ex.breast.in, -0.9, -0.5);
		bra.cleavage = adjust(ex.breast.cleavage, -0.6, 0);

		ctx.beginPath();
		drawPoints(ctx,
			bra.top,
			bra.out,
			bra.tip,
			bra.bot,
			bra.inner,
			bra.cleavage,
			bra.top
		);
		ctx.fill();
		ctx.stroke();
	}		
}


export function calcCorset(ex){
	let topIn;
	let topOut;
	let mid;
	let botOut;
	let botIn
	let waist = ex.waist;
	
	if(!ex.breast){
		topOut = clone(ex.armpit);
		topIn = {
			x:-0.2,
			y:topOut.y-4
		};
		topOut.cp1 = {
			x: topOut.x * 0.5 + topOut.x * 0.5,
			y: topIn.y
		};
	}else{
		let temp = getLimbPointsBellowPoint(ex.breast.cleavage,false,ex.armpit,ex.waist);
		topOut = temp[0];
		topIn = {
			x: -0.1,
			y: ex.breast.cleavage.y
		};
	}
	
	
	if(this.waistCoverage >= 0){
		let temp = splitCurve(1 - this.waistCoverage, ex.waist, ex.hip);
		botOut = temp.left.p2;
	}else{
		mid = ex.hip;
		let temp = splitCurve( Math.abs(this.waistCoverage), ex.hip, ex.thigh.out);
		botOut = temp.left.p2;
	};
	
	botIn = {
		x:-0.2,
		y:botOut.y-7
	};
	
		botIn.cp2 = {
			x: (botIn.x + botOut.x) * 0.5,
			y: botIn.y
		};
			
		//TODO - bottom curve could be adjusted by manipulating with this point
		botIn.cp1 = {
			x: botOut.x * 0.75,
			y: botIn.cp2.y + 7,
		};
				
	return{
		topIn,
		topOut,
		waist,
		mid,
		botOut,
		botIn
	};
}


export class CorsetPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		const {topIn,topOut,waist,mid,botOut,botIn} = calcCorset.call(this, ex);

		ctx.beginPath();
		drawPoints(ctx,	
			topIn,
			topOut,
			waist,
			mid,
			botOut,
			botIn,
		);
		ctx.fill();	
		ctx.stroke();
		
		//LACING
		if(this.lacing && this.knots > 0){
			const inTop = adjust(topIn,-3,-1);
			const inBot = adjust(botIn,-3,1);
			const outTop = adjust(topIn,3,-1);
			const outBot = adjust(botIn,3,1);
			
			let lacing = getLacingPoints(inBot,inTop,outBot,outTop,this.knots);

			ctx.strokeStyle = this.highlight; 
			ctx.beginPath();
			drawPoints(ctx, ...lacing.inner, breakPoint,...lacing.outer);
			ctx.stroke();	
		}
		
    }
}


export class HalfCorsetPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
			
		let {topIn,topOut,waist,mid,botOut,botIn} = calcCorset.call(this, ex);

		this.topCoverage = clamp(this.topCoverage, 0, 1);

		let temp = splitCurve(1-this.topCoverage,ex.armpit,ex.waist);
		topOut =  extractPoint(temp.left.p2); 
		waist =  temp.right.p2; 
	
		topIn = {
				x: -0.2,
				y:topOut.y - 4
			};
			
		topOut.cp1 = {
			x: (topOut.x + topIn.x) * 0.5,
			y: topIn.y
		};		
	
		ctx.beginPath();
		drawPoints(ctx,	
			topIn,
			topOut,
			waist,
			mid,
			botOut,
			botIn, 
		);
		ctx.fill();	
		ctx.stroke();
		
		//lacing
		if(this.lacing && this.knots > 0){
			const inTop = adjust(topIn,-3,-1);
			const inBot = adjust(botIn,-3,1);
			const outTop = adjust(topIn,3,-1);
			const outBot = adjust(botIn,3,1);
			const lacing = getLacingPoints(inBot,inTop,outBot,outTop,this.knots);

			ctx.strokeStyle = this.highlight; 
			ctx.beginPath();
			drawPoints(ctx, ...lacing.inner, breakPoint, ...lacing.outer);
			ctx.stroke();	
		}
		
    }
}

/* */

export class Corset extends Top { //dress?
    constructor(...data) {
        super({
			clothingLayer  : Clothes.Layer.OUTER, //TODO
			waistCoverage: 0,
			thickness: 1,
			lacing: true,
			knots: 6,
			highlight: "hsla(0, 0%, 52%, 1)",
        }, ...data);
    }

    stroke() {
        return "#000";
    }

    fill() {
        return "hsl(0,10%,20%)";
    }

    get partPrototypes() {
        return [
			{ 
                side: null,  
                Part: CorsetBreastPart
            },{ 
                side: null,  
                Part: CorsetPart
            },

        ];
    }
}	

export class HalfCorset extends Top { //dress?
    constructor(...data) {
        super({
			clothingLayer  : Clothes.Layer.OUTER, //TODO
			topCoverage: 0.6,
			waistCoverage: 0,
			thickness: 1,
			knots: 6,
			lacing: true,
			highlight: "hsla(0, 0%, 52%, 1)",
        }, ...data);
    }

    stroke() {
        return "#000";
    }

    fill() {
        return "hsl(0,10%,20%)";
    }

    get partPrototypes() {
        return [{ 
                side: null,  
                Part: HalfCorsetPart
            },
        ];
    }
}