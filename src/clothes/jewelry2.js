import {Clothes, ClothingPart, Clothing, processClothingPartLocation} from "./clothing";
import {Layer} from "../util/canvas";
import {locateRelativeToDrawpoint, Location} from "..";
import {
    drawPoints,
	drawCircle,
	adjust,
	extractPoint,
	breakPoint,
	reflect,
	splitCurve,
	rad
} from "drawpoint/dist-esm";

import {Piercing,Jewelry} from "./jewelry";

import {
	polar2cartesian
} from "../util/auxiliary";
 
 
 
export class BellyPiercingSimplePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+torso",
            reflect   : false,
			aboveParts: ["parts torso", "decorativeParts torso"]
        }, {
            radius: 1.3,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);

		const center = ex.bellybutton.bot;
		drawStud(center, this.radius); 
		
    }
}


export class BellyPiercingAdvancedPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+torso",
            reflect   : false,
			aboveParts: ["parts torso", "decorativeParts torso"]
        }, {
            thickness: 0.5,
			radius: 1.3,
			secondaryRadius: 1,
			distance: 1,
			above: true,
			above2: true,
			bellow: false,
			bellow2: false,
			chain: true,
			belt: false,
			link: false,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);

		const center = ex.bellybutton.bot;
		const originalFill = this.fill;
		

		function chain(center,x,y,thickness){
			center = extractPoint(center);
			
			ctx.setLineDash([2,2]);
			ctx.lineWidth = thickness;
			
			let top = adjust(center,x,0);
			let bot = adjust(center,x,y);
						
			ctx.beginPath();
			drawPoints(ctx,top,bot);
			ctx.stroke();
		}
			
			
		if(this.belt){
			let mid = {x:ex.bellybutton.bot.x,y:ex.bellybutton.bot.y};
			
			let temp = splitCurve(0.7,ex.waist,ex.hip);
			
			let left = extractPoint(temp.left.p2);
			left.cp1 = {x:((left.x+mid.x)*0.4),y:ex.hip.y};
				
			let right = reflect(left);
			right.cp1 = reflect(left.cp1);
		
			ctx.setLineDash([2, 2])
			ctx.lineWidth = 0.6;
			ctx.beginPath();
			drawPoints(ctx,mid,left,breakPoint,mid,right);
			ctx.stroke();
		}	
		
		if(this.chain){
			chain(center,0.6,-7,this.thickness)
			chain(center,-0.6,-9,this.thickness)
				 
		};
			
		if(this.link){
			let bot = extractPoint(adjust(center,0,-6 * this.distance));
			ctx.lineWidth = 0.6;
			ctx.setLineDash([2, 2]);			
			ctx.beginPath();
			drawPoints(ctx,center,bot);
			ctx.stroke();
				 
		};
		
		if(this.above){
			drawStud(ctx, 
				adjust(center,0,3.5 * this.distance),  
				0.7 * this.secondaryRadius
			); 
		}
		
		if(this.above2){
			drawStud(ctx, 
				adjust(center,0,6 * this.distance),  
				0.5 * this.secondaryRadius
			); 
		};
			
		if(this.bellow){
			drawStud(ctx, 
				adjust(center,0,-3.5 * this.distance),  
				0.7 * this.secondaryRadius
			); 
		}
		
		if(this.bellow2){
			drawStud(ctx, 
				adjust(center,0,-6 * this.distance),  
				0.5 * this.secondaryRadius
			); 
		};
		
		drawStud(ctx, center, this.radius); 
		
    }
}

export class NipplePiercingPart extends ClothingPart {
    constructor(...data) {
        super({
			layer     : Layer.GENITALS,
            loc       : "+torso",
            reflect   : true,
			aboveParts: ["parts chest"],
        }, {
			bar: true,
			radius: 0.6,
			distance: 1.6, 
			rotation: 0,
			
			secondBar: false, 
			shield: false, 
			
			ring: true,
			ringRadius: 1.4,
			ringOpenTop: 60,
			ringOpenBot: 0,
			ringEndpointRadius: 0.6,
			thickness: 0.3,
			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);

		const center = extractPoint(ex.chest.nipples);
		if(this.bar){
			drawBar(ctx, center, this.radius, this.distance, this.rotation)
		};
		
		if(this.secondBar){
			drawBar(ctx, center, this.radius, this.distance, this.rotation + 90)
		};
		
		if(this.ring){
			drawRing(ctx, adjust(center, 0, -this.ringRadius), this.ringRadius, this.ringOpenTop, this.rotation, this.ringOpenBot, this.ringEndpointRadius);
		};
		if(this.shield){
			const points = drawCircle(center, this.distance - 0.1); 
			ctx.beginPath();
			drawPoints(ctx, ...points);
			ctx.stroke();		
		};
		
    }
}


export class StudPart2 extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.BELOW_HAIR,
            loc       : `${Location.NOSE}`,
            reflect   : false,
            aboveParts: [`faceParts ${Location.NOSE}`],
        }, {
            radius: 0.51,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
         Clothes.simpleStrokeFill(ctx, ex, this);
		 
		const center = locateRelativeToDrawpoint(ex, this.relativeLocation);
        drawStud(ctx, center, this.radius, this.fill);	
    }
}


export class HorseshoePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.BELOW_HAIR,
            loc       : `${Location.NOSE}`,
            reflect   : false,
            aboveParts: [`faceParts ${Location.NOSE}`],
        }, {
            radius: 0.51,
			ringRadius: 1.2,
			ringOpenTop: 140,
			ringOpenBot: 0,
			thickness: 0.3,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
         Clothes.simpleStrokeFill(ctx, ex, this);
		 
		const center = locateRelativeToDrawpoint(ex, this.relativeLocation);
		drawRing(ctx, adjust(center, 0, -this.ringRadius), this.ringRadius, this.ringOpenTop, this.rotation, this.ringOpenBot, this.radius);
   
    }
}


export class BridgePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.BELOW_HAIR,
            loc       : `${Location.NOSE}`,
            reflect   : false,
            aboveParts: [`faceParts ${Location.NOSE}`],
        }, {
            radius: 0.4,
			distance: 0.9,
			rotation: 0,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
         Clothes.simpleStrokeFill(ctx, ex, this);
		 
		const center = locateRelativeToDrawpoint(ex, this.relativeLocation);
		drawBar(ctx, center, this.radius, this.distance, this.rotation)		
		
    }
}


function drawStud(ctx, center, radius, fill){
	const originalFill = ctx.fillStyle;
	
	if(fill){
		ctx.fillStyle = fill;
	};
	
	const circle = drawCircle(center, radius); 
	ctx.beginPath();
	drawPoints(ctx,
		...circle
	);
	ctx.fill();	
	
	const shine = drawCircle( adjust(center, -0.3 * radius, 0.3 * radius), radius * 0.3); 
	ctx.fillStyle = "white";
	ctx.beginPath();
	drawPoints(ctx,
		...shine
	);
	ctx.fill();	
	
	ctx.fillStyle = originalFill;
}


export function drawRing(ctx, center, radius, openTop, rotation = 0, openBot = 0, endpointRadius){
	/*
		openTop - the part of piercing hidden under skin
		openBot - visible open part of the piercing, intended for horseshoe designs
		both in degress
	*/

	const rot = rad(rotation);
	const open = rad(openTop / 2)
	const start = rad(-90) - rot + open;
	const end = rad(270) - rot - open;
	//remember - the start of the ring is on the top [0,1] but radians are counted from [1,0], 
	
	if(!openBot){
		ctx.beginPath();
		//TODO 
		try{
			ctx.arc(center.x, center.y, radius, start, end);
		}catch{
			ctx.arc(center.x, center.y, -radius, start, end);
		}
		ctx.stroke();					
		
		if(endpointRadius > 0){
			const mid = polar2cartesian(radius, rad(-90) + rot, center);
			drawStud(ctx, mid, endpointRadius)
		}
		
	}else{
		const open = rad(openBot / 2)
		const startMid = rad(90) - rot - open;
		const endMid = rad(90) - rot + open;
	
		//TODO
		try{
			ctx.beginPath();
			ctx.arc(center.x, center.y, radius, start, startMid);
			ctx.stroke();		
			
			ctx.beginPath();
			ctx.arc(center.x, center.y, radius, endMid, end);
			ctx.stroke();
		}catch{
			ctx.beginPath();
			ctx.arc(center.x, center.y, -radius, start, startMid);
			ctx.stroke();		
			
			ctx.beginPath();
			ctx.arc(center.x, center.y, -radius, endMid, end);
			ctx.stroke();
		}
		
		
		if(endpointRadius > 0){
			const midStart = polar2cartesian(radius, rad(-90) + rot - open, center);
			drawStud(ctx, midStart, endpointRadius)
			
			const midEnd = polar2cartesian(radius, rad(-90) + rot + open, center);
			drawStud(ctx, midEnd, endpointRadius)
		}
		
		
	}
};


export function drawBar(ctx, center, radius, distance, rotation, radius2){
	const first = polar2cartesian(distance, rad(0) + rad(rotation), center);
	drawStud(ctx, first, radius);	
		
	const second = polar2cartesian(distance, rad(180) + rad(rotation), center);
	if(radius2 === undefined){
		radius2 = radius;
	};
	drawStud(ctx, second, radius2);
	
}


	
/*

*/		


	
export class BellyPiercingSimple extends Jewelry {
    constructor(...data) {
        super({
        }, ...data);
    }

	stroke() {
        return "lime";
    }
	
    fill() {
        return "red";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BellyPiercingSimplePart,
            },
        ];
    }
}

export class BellyPiercingAdvanced extends Jewelry {
    constructor(...data) {
        super({
        }, ...data);
    }

	stroke() {
        return "lime";
    }
	
    fill() {
        return "red";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BellyPiercingAdvancedPart,
            },
        ];
    }
}

export class NipplePiercing extends Jewelry {
    constructor(...data) {
        super({
			clothingLayer: Clothes.Layer.BASE,
        }, ...data);
    }

	stroke() {
        return "blue";
    }
	
    fill() {
        return "red";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: NipplePiercingPart,
            },
        ];
    }
}

export class StudPiercing2 extends Piercing {
    constructor(...data) {
        super({
            relativeLocation: {
                drawpoint: "nose.out",
                dx       : -3,
                dy       : 1.15
            },
            loc             : `+${Location.NOSE}`,
            requiredParts   : "faceParts",
			 
        },...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: StudPart2,
            },
        ];
    }
}

export class HorseshoePiercing extends Piercing {
    constructor(...data) {
        super({
            relativeLocation: {
                drawpoint: "nose.center",
                dx       : 0,
                dy       : 1.1
            },
            loc             : `+${Location.NOSE}`,
            requiredParts   : "faceParts",
			 
        },...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: HorseshoePart,
            },
        ];
    }
}

export class Bridge extends Piercing {
    constructor(...data) {
        super({
            relativeLocation: {
                drawpoint: "nose.top",
                dx       : -0.3,
                dy       : -1.66
            },
            loc             : `+${Location.NOSE}`,
            requiredParts   : "faceParts",
			 
        },...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BridgePart,
            },
        ];
    }
}