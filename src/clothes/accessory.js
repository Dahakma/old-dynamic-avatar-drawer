import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {
    drawPoints,
    extractPoint,
    reflect,
    breakPoint, splitCurve, simpleQuadratic, adjust, drawCircle,
	interpolateCurve,
} from "drawpoint/dist-esm";

import {
    perpendicularPoint,
} from "../util/auxiliary";
import {setStrokeAndFill} from "..";

export class FaceAccessoryPart extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.BELOW_HAIR,
                loc       : "+head",
                aboveParts: ["eyelid", "brow", "eyelash", "decorativeParts head"]
            },
            ...data);
    }
}

export class GagStrapPart extends FaceAccessoryPart {
    constructor(...data) {
        super({
                reflect: true,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        const right = extractPoint(ex.chin.out);

        const [, left, , ,] = calcGag(ex);
        // const left = adjust(extractPoint(ex.lips.out), -1, -0.7);

        left.cp1 = simpleQuadratic(right, left, 0.5, 1);
        left.cp2 = null;

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx, right, left);
        ctx.stroke();
    }
}

export class TopTriangularStrap extends FaceAccessoryPart {
    constructor(...data) {
        super({
                layer            : Layer.ABOVE_HAIR,
                reflect          : true,
                branchOutDistance: 18,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        const right = extractPoint(ex.chin.out);
        const left = {x: 0, y: ex.skull.y - this.branchOutDistance};
        left.cp1 = simpleQuadratic(right, left, 0.5, 1);

        const top = extractPoint(ex.skull);

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx, right, left, top);
        ctx.stroke();
    }
}

export class RingGagPart extends FaceAccessoryPart {
    constructor(...data) {
        super({
                ringStroke   : "#393939",
                ringThickness: 0.5,
                spiderLegs   : false,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        setStrokeAndFill(ctx,
            {
                stroke: this.ringStroke,
                fill  : this.ringStroke
            },
            ex);
        ctx.lineWidth = this.ringThickness;

        // want to draw a circle, need to measure distances between mouth
        const pts = calcGag(ex);

        ctx.beginPath();
        drawPoints(ctx, ...pts);
        ctx.stroke();

        if (this.spiderLegs) {
            const spiderPts = [];
            for (let i = 0; i < pts.length - 1; ++i) {
                const sp = splitCurve(0.5, pts[i], pts[i + 1]);
                const start = sp.left.p2;

                let mid = null;
                if (i % 2 === 0) {
                    mid = {x: pts[i + 1].x * 1.2, y: pts[i].y};
                } else {
                    mid = {x: pts[i].x * 1.2, y: pts[i + 1].y};
                }

                let end = null;
                if (i in [0, 1]) {
                    end = {x: mid.x + 1, y: mid.y};
                } else {
                    end = {x: mid.x - 1, y: mid.y};
                }

                spiderPts.push(start, mid, end, breakPoint);
            }
            ctx.beginPath();
            drawPoints(ctx, ...spiderPts);
            ctx.stroke();
        }
    }
}

export class BallGagPart extends FaceAccessoryPart {
    constructor(...data) {
        super({
                ballFill: "red",
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        setStrokeAndFill(ctx,
            {
                stroke: this.ballFill,
                fill  : this.ballFill
            },
            ex);

        // want to draw a circle, need to measure distances between mouth
        const [top, right, bot, left, _] = calcGag(ex);

        ctx.beginPath();
        drawPoints(ctx, top, right, bot, left, top);
        ctx.fill();
    }
}

function calcGag(ex) {
    const center = {x: 0, y: ex.lips.top.y * 0.53 + ex.lips.bot.y * 0.47};
    // want to draw a circle, need to measure distances between mouth
    const radius = 0.7 * Math.min((ex.lips.top.y - ex.lips.bot.y) / 2, ex.lips.out.x);
    return drawCircle(center, radius);
}

export class BlindFoldPart extends FaceAccessoryPart {
    constructor(...data) {
        super({
                reflect: true,
            },
            {
                width            : 6,
                splitAlongSkull  : 0.9,
                fromSkullDistance: 16,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        let sp = splitCurve(0.96, ex.skull, ex.skull.side);
        const right = extractPoint(sp.right.p1);
        const left = {x: 0, y: ex.skull.y - this.fromSkullDistance};
        left.cp1 = {x:right.x-0.5, y:right.y-2};
        left.cp2 = {x:left.x+2, y:left.y};

        const botLeft = {x: 0, y: left.y - this.width};

        sp = splitCurve(this.splitAlongSkull, ex.skull.side, ex.skull.bot);
        const botRight = sp.right.p1;

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx, right, left, botLeft, botRight, right);
        ctx.fill();

    }
}



export class FaceMaskPart extends FaceAccessoryPart {
    constructor(...data) {
        super({
                 layer            : Layer.HAIR,
				 reflect: true,
            },
            {
				noseCoverage: 0.8,
				skullCoverage: 0,
				connectEars: true,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		const nose = {
			x: 0,
			y: (ex.eyes.in.y - 2) - 6 * (1 - this.noseCoverage),
		}
		
		const sp = splitCurve(1 - this.skullCoverage, ex.skull.side, ex.skull.bot);
		const outTop = sp.left.p2;
		const outBot = sp.right.p2;
 
		//drawn before CPs are changed
		if(this.connectEars){
			ctx.beginPath();
			drawPoints(ctx, 
				ex.skull.side,
				outTop,			
			);	
			ctx.stroke();
		};
		
		
		outTop.cp1 = {
			x: nose.x + 2,
			y: nose.y + 0
		}
		
		outTop.cp2 = {
			x: nose.x + 4,
			y: nose.y - 4
		}
				
        
        ctx.beginPath();
        drawPoints(ctx, 
				nose,
				outTop,
				outBot,
				ex.chin.out,
				ex.chin.bot,

		);
		ctx.fill();
		ctx.stroke();
		
		
    }
}

//TODO - cowboy mask, surgical mask

export class VeilPart extends FaceAccessoryPart {
    constructor(...data) {
        super({
                 layer            : Layer.HAIR,
				 reflect: true,
            },
            {
				thickness: 0.8,
				noseCoverage: 0.99,
				skullCoverage: 0.45,
				width: -1,
				length: 12,
				connectEars: false,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		const nose = {
			x: 0,
			y: (ex.eyes.in.y - 2) - 6 * (1 - this.noseCoverage),
		}
		
		const sp = splitCurve(1 - this.skullCoverage, ex.skull.side, ex.skull.bot);
		const outTop = sp.left.p2;
		const outMid = sp.right.p2;
 
		//drawn before CPs are changed
		if(this.connectEars){
			ctx.beginPath();
			drawPoints(ctx, 
				ex.skull.side,
				outTop,			
			);	
			ctx.stroke();
		};
		
		
		outTop.cp1 = {
			x: nose.x + 2,
			y: nose.y + 0
		}
		
		outTop.cp2 = {
			x: nose.x + 10,
			y: nose.y - 4
		}
			
		const outBot = {
			x: ex.skull.bot.x + this.width,
			y: ex.chin.bot.y - this.length,
		}
		
		const bottom = {
			x: -0.1,
			y: ex.chin.bot.y - this.length,
		}
		
		//bottom curve
		outBot.y += 2;
		bottom.y += -2;
		bottom.cp1 = {
			x: (bottom.x + outBot.x) * 0.5,
			y: bottom.y
		};
		
        ctx.beginPath();
        drawPoints(ctx, 
				nose,
				outTop,
				outMid,
				outBot,
				bottom

		);
		ctx.fill();
		
		
		ctx.beginPath();
        drawPoints(ctx, 
				nose,
				outTop,
		);
		ctx.stroke();
		
		
    }
}


export class GlassesPart extends FaceAccessoryPart {
    constructor(...data) {
        super({
                reflect: true,
            },
            {
                eccentricity: 5,
                height      : 3,
                thickness   : 0.5,
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);

        let ear = extractPoint(ex.ear.mid);
        ear.y += 1;
        ear.x -= 1;

        let outerControlPoint = extractPoint(ex.eyes.out);
        outerControlPoint.x += 1;

        let toEar = extractPoint(ex.ear.mid);
        toEar.x -= 2;
        toEar.y += 2;

        let innerControlPoint = extractPoint(ex.eyes.in);
        innerControlPoint.x -= 1.8;

        const eccentricity = this.eccentricity;
        const height = this.height;
        //let halfControlPoint = {x:findBetween(innerControlPoint.x,outerControlPoint.x),y:findBetween(innerControlPoint.y,outerControlPoint.y) };

        let innerControlPointTop = {x: innerControlPoint.x, y: innerControlPoint.y};
        innerControlPointTop.cp1 = perpendicularPoint(innerControlPoint, outerControlPoint, 0.5, height);
        innerControlPointTop.cp1.x += eccentricity;
        innerControlPointTop.cp2 = perpendicularPoint(innerControlPoint, outerControlPoint, 0.5, height);
        innerControlPointTop.cp2.x += -eccentricity;

        let innerControlPointBot = {x: innerControlPoint.x, y: innerControlPoint.y};
        innerControlPointBot.cp1 = perpendicularPoint(innerControlPoint, outerControlPoint, 0.5, -height);
        innerControlPointBot.cp1.x += eccentricity;
        innerControlPointBot.cp2 = perpendicularPoint(innerControlPoint, outerControlPoint, 0.5, -height);
        innerControlPointBot.cp2.x += -eccentricity;

        let toNose = reflect(innerControlPoint);

        toNose.cp1 = {
            y: innerControlPoint.y + 2,
            x: 0
        };

        ctx.beginPath();
        drawPoints(ctx, outerControlPoint, innerControlPointTop, breakPoint, outerControlPoint, innerControlPointBot);
        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx, toEar, outerControlPoint, innerControlPointTop, breakPoint, outerControlPoint, innerControlPointBot, toNose);
        ctx.stroke();
    }
}

/*
export class BeltPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+torso",
			reflect		:true, 
            aboveParts: ["clothingParts leg","clothingParts groin"],
		},
		{	
			waistCoverage: 0.33,
			width:4,
			curve:-5,
			buckle:2,
			highlight:"#cdc331",
        },
		...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		let temp;
		
		//TOP
		let topOut;
		if(this.waistCoverage>0){
			let temp = splitCurve((1-this.waistCoverage),ex.waist,ex.hip);	
			topOut = extractPoint(temp.left.p2);
		}else{
			let temp = splitCurve(Math.abs(this.waistCoverage),ex.hip,ex.thigh.out);	
			topOut = extractPoint(temp.left.p2);
		}
	
		//BOT
		let botOut;
		if(topOut.y-this.width>ex.hip.y){
			botOut = interpolateCurve(ex.waist, ex.hip,{
				x: null,
				y: topOut.y-this.width
			});
			botOut=botOut[0];
		}else{
			botOut = interpolateCurve(ex.hip, ex.thigh.out,{
				x: null,
				y: topOut.y-this.width
			});
			botOut=botOut[0];
		}
		
		let topIn = {
			x: -0.1,  
			y: topOut.y + this.curve
		};
		
		let botIn = {
			x: -0.1,  
			y:	topIn.y - this.width
		};

		
		//BUCKLE
			temp = splitCurve(this.buckle/10,topIn,topOut);	
		let topMid = extractPoint(temp.left.p2);
		topIn = {
			x:topIn.x,
			y:topMid.y
		};
	
	 
			temp = splitCurve(this.buckle/10,botIn,botOut);	
		let botMid = {
			x:topMid.x,
			y:temp.left.p2.y
		};
		botIn = {
			y:botMid.y,
			x:topIn.x,
		};
		
		ctx.beginPath();
        drawPoints(ctx, 
			topIn,
			topMid,
			topOut,
			botOut,
			botMid,
			botIn
		);
        ctx.fill();
        ctx.stroke();
		
		ctx.fillStyle=this.highlight;
		const adjustement=1;
		ctx.beginPath();
        drawPoints(ctx, 
			adjust(topIn,-0.2,adjustement),
			adjust(topMid,0,adjustement),
			adjust(botMid,0,-adjustement),
			adjust(botIn,-0.2,-adjustement)
		);
        ctx.fill();
        ctx.stroke();
	}
}
*/


export class SimpleBeltPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.MIDRIFT,
            loc       : "+torso",
			reflect		:true, 
            aboveParts: [ "parts leg","clothingParts groin", "parts chest", "clothingParts chest", "decorativeParts chest"  /*"clothingParts leg","clothingParts groin"*/],
			aboveSameLayerParts: ["clothingParts torso"],
//			 aboveParts: [ "parts leg","clothingParts groin", "parts torso", "decorativeParts torso" , "decorativeParts torso","parts chest", "clothingParts chest", "decorativeParts chest"  /*"clothingParts leg","clothingParts groin"*/],
		},
		{	
			waistCoverage: 0.2,
			beltWidth:4,
			beltCurve:-1,
			highlight:"#cdc331",
        },
		...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		const {inTop,outTop,outMid,outBot,inBot} = calcBelt.call(this, ex);
		
		ctx.beginPath();
        drawPoints(ctx, 
			inTop,
			outTop,
			outMid,
			outBot,
			inBot,
		);
        ctx.fill();
        ctx.stroke();

	}
}


export class BeltPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.MIDRIFT,
            loc       : "+torso",
			reflect		:true, 
            aboveParts: [ "parts leg","clothingParts groin", "parts chest", "clothingParts chest", "decorativeParts chest"  /*"clothingParts leg","clothingParts groin"*/],
			aboveSameLayerParts: ["clothingParts torso"],
		},{	
			waistCoverage: 0.2,
			beltWidth:4,
			beltCurve:-1,
			
			highlight: "yellow",
			buckleWidth: 3,
			buckleHeight: 0.5,
			showBuckle: true,
        },
		...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		const {
			inTop, outTop, outMid, outBot, inBot,
			buckleInTop, buckleOutTop, buckleOutBot, buckleInBot,
		} = calcBeltBuckle.call(this, ex);
		
		ctx.beginPath();
        drawPoints(ctx, 
			inTop,
			outTop,
			outMid,
			outBot,
			inBot,
		);
        ctx.fill();
        ctx.stroke();

		if(!this.showBuckle){
			return;
		}
		
        ctx.fillStyle = this.highlight;
		ctx.beginPath();
        drawPoints(ctx, 
			buckleInTop,
			buckleOutTop,
			buckleOutBot,
			buckleInBot, 
		);
        ctx.fill();
        ctx.stroke();
		
	}
}


export function calcBelt(ex) {
	let outTop;
	let outMid
	let outBot;
	let inTop;
	let inBot;
	let temp;
	
	if(this.beltWidth < 0)this.beltWidth = 0;
	
	//TOP
	if(this.waistCoverage>0){
		temp = splitCurve((1-this.waistCoverage),ex.waist,ex.hip);	
		outTop = extractPoint(temp.left.p2);
	}else{
		temp = splitCurve(Math.abs(this.waistCoverage),ex.hip,ex.thigh.out);	
		outTop = extractPoint(temp.left.p2);
	};
	
	//BOT
	//both outTop and outBot above hip
	if(outTop.y-this.beltWidth>ex.hip.y){
		outBot = interpolateCurve(temp.left.p2, temp.right.p2,{
			x: null,
			y: outTop.y - this.beltWidth
		});
		outBot = splitCurve(outBot[0].t,temp.left.p2, temp.right.p2);
		outBot = outBot.left.p2;
	//outTop above, outBot bellow
	}else if(outTop.y > ex.hip.y){
		outMid = temp.right.p2; 
		outBot = interpolateCurve(ex.hip, ex.thigh.out,{
			x: null,
			y: outTop.y - this.beltWidth
		});
		outBot = splitCurve(outBot[0].t,ex.hip,ex.thigh.out);
		outBot = outBot.left.p2;
	//both are bellow
	}else{
		outBot = interpolateCurve(temp.left.p2, temp.right.p2,{
			x: null,
			y: outTop.y - this.beltWidth
		});
		outBot = splitCurve(outBot[0].t,temp.left.p2, temp.right.p2);
		outBot = outBot.left.p2;
	};
	
	//IN
	const waistCurve = outTop.y - ex.hip.y;
	inTop = {
		x: -0.2,  
		y: ex.pelvis.y + waistCurve * 1.2 + this.beltCurve,
	};
		
	inBot = {
		x: -0.2,  
		y:	inTop.y - this.beltWidth
	};

	//CURVE
	outTop.cp1 = {
		x: (outTop.x + inTop.x) * 0.5,
		y: inTop.y
	};
	
	inBot.cp1 = {
		x: (inBot.x + outBot.x) * 0.5,
		y: inBot.y
	};
	
	return {
		inTop,
		outTop,
		outMid,
		outBot,
		inBot,
	};
		
		
};


export function calcBeltBuckle(ex) {
	const {inTop,outTop,outMid,outBot,inBot} = calcBelt.call(this, ex);
	
	const t = interpolateCurve({x: 0, y: inTop.y}, outTop,{
		x: this.buckleWidth,
		y: null, 
	})[0].t;
	
	let temp;
	
	temp  = splitCurve(t, inTop, outTop);
	
	const buckleOutTop =  temp.left.p2;
		
	temp = splitCurve(1 - t, outBot, inBot); 
		
	const buckleOutBot = extractPoint(temp.left.p2);
	const buckleInBot = temp.right.p2;
	
	
	return {
		inTop,
		outTop,
		outMid,
		outBot,
		inBot,
		
		buckleInTop:  adjust(inTop,       -0.5,  this.buckleHeight),
		buckleOutTop: adjust(buckleOutTop,   0,  this.buckleHeight),
		buckleOutBot: adjust(buckleOutBot,   0, -this.buckleHeight),
		buckleInBot:  adjust(buckleInBot, -0.5, -this.buckleHeight),	
	};		
}







export class ApronPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.MIDRIFT,
            loc       : "+torso",
			reflect		:true, 
            aboveSameLayerParts: ["clothingParts torso"],
			aboveParts: [ "parts leg","clothingParts groin",  "parts chest", "clothingParts chest", "decorativeParts chest"  /*"clothingParts leg","clothingParts groin"*/],
		},
		{	
			waistCoverage: 0.5,
			beltWidth:3,
			beltCurve:-1,
			coverage:0.9,
			length:50,
			curveX:20,
			curveY:0,
			highlight:"#cdc331",
        },
		...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		const {inTop,outTop,outMid,outBot,inBot} = calcBelt.call(this, ex);
		let temp = splitCurve(this.coverage, inTop, outTop);
		const top =  temp.left.p2;
		let bottom = {
			x:inBot.x,
			y:inBot.y - this.length
		}
		bottom.cp1 = {
			x: top.x + this.curveX,
			y: bottom.y + this.curveY
		}
			
		ctx.beginPath();
        drawPoints(ctx, 
			inTop,
			top,
			bottom
		);
        ctx.fill();
        ctx.stroke();


	}
}


/**
 * Base Clothing classes
 */
export class Accessory extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.MID,
        }, ...data);
    }
}


/**
 * Concrete Clothing classes
 */
export class Glasses extends Accessory {
    constructor(...data) {
        super(...data);
    }

    fill() {
        return "#a2a2a2";
    }

    stroke() {
        return "#1e1e1e";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: GlassesPart,
            },
        ];
    }
}


export class Gag extends Accessory {
    constructor(...data) {
        super({
            /**
             * How much the gag forces the lips to part, similar in effect to some facial expressions
             */
            lipParting: 60,
            /**
             * Controls the width of the straps
             */
            thickness : 1.5,
        }, ...data);
        // resolve dynamics between modifiers
        this.Mods = Object.assign({
            lipParting: this.lipParting,
        }, this.Mods);
    }

    fill() {
        return "#a2a2a2";
    }

    stroke() {
        return "#1e1e1e";
    }

}

export class SimpleRingGag extends Gag {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: GagStrapPart,
            },
            {
                side: null,
                Part: RingGagPart,
            },
        ];
    }
}

export class SimpleBallGag extends Gag {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: GagStrapPart,
            },
            {
                side: null,
                Part: BallGagPart,
            },
        ];
    }
}

export class MediumRingGag extends Gag {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: GagStrapPart,
            },
            {
                side: null,
                Part: TopTriangularStrap,
            },
            {
                side: null,
                Part: RingGagPart,
            },
        ];
    }
}

export class MediumBallGag extends Gag {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: GagStrapPart,
            },
            {
                side: null,
                Part: TopTriangularStrap,
            },
            {
                side: null,
                Part: BallGagPart,
            },
        ];
    }
}

export class BlindFold extends Accessory {
    constructor(...data) {
        super(...data);
    }

    fill() {
        return "#1e1e1e";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BlindFoldPart,
            },
        ];
    }
}


export class FaceMask extends Accessory {
    constructor(...data) {
        super(...data);
    }

    fill() {
       return "#3d455e";
    }

	stroke() {
        return "#282a33";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: FaceMaskPart,
            },
        ];
    }
}


export class Veil extends Accessory {
    constructor(...data) {
        super(...data);
    }

    fill() {
       return "hsla(334.0,68.8%,72.4%,0.75)";
    }

	stroke() {
        return "#6f0e6c";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: VeilPart,
            },
        ];
    }
}

export class Belt extends Accessory {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.OUTER,
        },...data);
    }

	fill() {
        return "#464646";
    }
	
	stroke() {
       return "#1e1e1e";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: BeltPart,
            },
        ];
    }
}

export class SimpleBelt extends Accessory {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.OUTER,
        },...data);
    }

	fill() {
        return "#464646";
    }
	
	stroke() {
       return "#1e1e1e";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: SimpleBeltPart,
            },
        ];
    }
}


export class Apron extends Accessory {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.OUTER,  // !!!
        },...data);
    }

	fill() {
        return "#ffffff";
    }
	
	stroke() {
       return "#1e1e1e";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: SimpleBeltPart,
            },{
                side: null,
                Part: ApronPart,
            },
        ];
    }
}