import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {connectEndPoints, coverNipplesIfHaveNoBreasts} from "../draw/draw";
import {Layer} from "../util/canvas";
import {
    extractPoint,
    drawPoints,
    splitCurve,
    breakPoint,
    clone,
	clamp,
    //none,
    adjust,
    reflect,
    interpolateCurve,
	simpleQuadratic,
} from "drawpoint/dist-esm";
import {
	getLimbPointsBellowPoint,
} from "../util/auxiliary";

import {Dress, calcDressBase} from "./dress"
//import {CorsetBreastPart} from "./corset"
import {HalterTopBreastPart, TubeTopBreastPart} from "./tops"
import {calcBra} from "./underwear";

export class MiniDressBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso", "parts leg", "decorativeParts leg"],
            aboveSameLayerParts: ["groin", "leg"],

        }, {
            cleavageOpeness : 0.3,
            cleavageCoverage: 0.3,
            sideLoose       : 0,
            legCoverage     : 0.24,
            legLoose        : 0,
            curveCleavageX  : 0,
            curveCleavageY  : 0,
            bustle          : false,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const {
            cleavageBot, cleavageTop, neck, collarbone, armpit, waist, hip, outerPoints, bottom,
        } = calcDressBase.call(this, ex);

        Clothes.simpleStrokeFill(ctx, ex, this);

		let topIn;
		let topOut;
		if(!ex.breast){
			topOut = clone(ex.armpit);
			topIn = {
				x:-0.2,
				y:topOut.y-4
			};
			topOut.cp1 = {
				x: topOut.x * 0.5 + topOut.x * 0.5,
				y: topIn.y
			};
		}else{
			let temp = getLimbPointsBellowPoint(ex.breast.cleavage,false,ex.armpit,ex.waist);
			topOut = temp[0];
			topIn = {
				x:-0.1,
				y:ex.breast.cleavage.y
			};
		}
	
	
	
        ctx.beginPath();
        drawPoints(ctx,
            topIn,
			topOut,
            ...outerPoints,
            bottom
        );
        ctx.fill();
		ctx.stroke();
    }
}

export class HalterTopMiniDressBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso", "parts leg", "decorativeParts leg"],
            aboveSameLayerParts: ["groin", "leg"],

        }, {
            cleavageOpeness : 0.3,
            cleavageCoverage: 0.3,
            sideLoose       : 0,
            legCoverage     : 0.24,
            legLoose        : 0,
            curveCleavageX  : 0,
            curveCleavageY  : 0,
            bustle          : false,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const {
            cleavageBot, cleavageTop, neck, collarbone, armpit, waist, hip, outerPoints, bottom,
        } = calcDressBase.call(this, ex);

        Clothes.simpleStrokeFill(ctx, ex, this);

		const topIn = {x : -0.1};
		let topOut = extractPoint(ex.armpit);
		let topMid;
	
		const temp = splitCurve(this.cleavageCoverage,ex.neck.cusp, ex.groin);
		if(temp.left.p2.y < ex.armpit.y){
			topIn.y = temp.left.p2.y;
			if(ex.breast){
				topMid = extractPoint(ex.breast.bot);
			};
		}else{
			topIn.y = ex.armpit.y;
		};
		
		
		 ctx.beginPath();
        drawPoints(ctx,
            topIn,
			topMid,
			topOut,
            ...outerPoints,
            bottom
        );
        ctx.fill();
		
		ctx.beginPath();
        drawPoints(ctx,
            topOut,
            ...outerPoints,
            bottom
        );
		ctx.stroke();
    }
}


export class MiniDressBreastPart extends ClothingPart {
	constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }
	
	renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		if (coverNipplesIfHaveNoBreasts(ex, ctx, this)) {
			if(this.showStrap){
				ctx.lineWidth = this.strapWidth; 
				ctx.beginPath();
				drawPoints(ctx, 
					ex.neck.cusp,
					extractPoint(ex.armpit)
				);
				ctx.stroke();
			}
            return;
        }
		
		const bra = calcBra(ex);
      
		bra.out.cp1 = simpleQuadratic(bra.top, bra.out, 0.4, 1);
		bra.top.cp1 = simpleQuadratic(ex.breast.cleavage, bra.top, 0.6, 2);
		
		//based on the dress to cover the dress:
		bra.bot = adjust(ex.breast.bot, 0, -0.6);
		bra.inner = adjust(ex.breast.in, -0.9, -0.5);
		bra.cleavage = adjust(ex.breast.cleavage, -0.6, 0);

		ctx.beginPath();
		drawPoints(ctx,
			bra.top,
			bra.out,
			bra.tip,
			bra.bot,
			bra.inner,
			bra.cleavage,
			bra.top
		);
		ctx.fill();
		ctx.stroke();
		
		
		if(this.showStrap){
			ctx.lineWidth = this.strapWidth; 
			ctx.beginPath();
			drawPoints(ctx, 
				ex.neck.cusp,
				extractPoint(bra.out)
			);
			ctx.stroke();
		}
	}		
}


/* */

export class MiniDress extends Dress {
    constructor(...data) {
        super({
			strapWidth: 1,
			showStrap: true,
		}, ...data);
    }

    stroke() {
        return "hsla(83, 90%, 31%, 1)";
    }

    fill() {
        return "hsla(83, 100%, 41%, 1)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: MiniDressBreastPart
            },{
                side: null,
                Part: MiniDressBasePart
            },
        ];
    }
}


export class MiniDress2 extends Dress {
    constructor(...data) {
        super({
			cleavageCoverage: 0.26,
			outerNeckCoverage: 1.4,
			innerNeckCoverage: 1.15,
			curveCleavageX: 6,
			curveCleavageY: -7,
			waistCoverage: 0.66,
			sideLoose: 0,
		}, ...data);
    }

    stroke() {
        return "hsla(12, 90%, 44%, 1)";
    }

    fill() {
        return "hsla(21, 74%, 51%, 1)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: HalterTopBreastPart,
            },{
                side: null,
                Part: HalterTopMiniDressBasePart
            },
        ];
    }
}


export class MiniDress3 extends Dress {
    constructor(...data) {
        super({
			strapWidth: 1,
			showStrap: true,
			chestCoverage: 0.3,
		}, ...data);
    }

    stroke() {
        return "hsla(183, 90%, 31%, 1)";
    }

    fill() {
        return "hsla(183, 100%, 41%, 1)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: TubeTopBreastPart
            },{
                side: null,
                Part: MiniDressBasePart
            },
        ];
    }
}