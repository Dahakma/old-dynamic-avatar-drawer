import {Clothes, ClothingPart, Clothing} from "./clothing";
import {shine} from "../draw/shading_part";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {
    reverseDrawPoint,
    splitCurve,
    simpleQuadratic,
    drawPoints,
    extractPoint,
    adjust,
    continueCurve,
    scale, scalePoints, rotatePoints, breakPoint
} from "drawpoint/dist-esm";

import {ShoePart, ShoeSidePart, Shoe} from "./shoes";
import {HeelPart, Heels} from "./heels";
import {calcSuperSocks} from "./super_socks";

import {
	getLimbPointsBellowPoint,
	getLimbPointsAbovePoint,
	findBetween,
	getLacingPoints,
	perpendicularPoint
	/*
	
	
	getLacingPoints
	*/
} from "../util/auxiliary";


export function calcShoeBase(ex) {
	//BASE
	const ankleOut = adjust(ex.ankle.outbot, this.tight, 0);
	const ankleIn = adjust(ex.ankle.inbot, -this.tight, 0);
	ankleIn.cp2.x -= 1 + this.tight;
	const toeOut = adjust(ex.toe.out, this.tight + 1,0);
	const toeIn = adjust(ex.toe.in, -this.tight, -this.tight);
		
	//TIP
	let temp = splitCurve(0.4,toeIn,ankleIn);
	const tipIn = temp.left.p2;
	const tipOut = extractPoint(toeOut);
	tipOut.cp1 = {
		x: findBetween(tipOut.x, tipIn.x),
		y: tipOut.y - (2 + this.shoeHeight) + this.tip,
	};
	
	return {
		ankleOut, 
		toeOut, 
		toeIn, 
		ankleIn,
		tipOut,
		tipIn,
    };
}

	
export function calcSneakers(ex) {
	const {
		ankleOut, 
		toeOut, 
		toeIn, 
		ankleIn,
		tipOut,
		tipIn,
	} = calcShoeBase.call(this, ex);
	
	//LACING 	
	const outTop = extractPoint(ex.ankle.out);
	const outBot = adjust(outTop, -2, -11);
	const inTop = extractPoint(ex.ankle.in);
	const inBot = adjust(inTop, -0.5, -11)
	const lacingPoints = getLacingPoints(outBot, outTop, inBot, inTop, this.crosses, -1);
	
	//TONGUE	
	const tongueOut = adjust(outTop, -1, 0);
	const tongueIn = adjust(inTop, 1, 0)
	tongueOut.cp1 = perpendicularPoint(outTop, tongueIn, 0.5, -this.tongue);
	
	return {
		ankleOut, 
		toeOut, 
		toeIn, 
		ankleIn,
		tipOut,
		tipIn,
		tongueOut,
		tongueIn,
		lacingPoints,
    };
}



export class SneakersPart extends HeelPart {
    constructor(...data) {
        super({},{
			tip: 0,
			tipColor: "", 
			highlight: "white",
			crosses: 3,
			laceThickness: 0.8,
			tongue: 6.6,

		},...data);
    }

	renderClothingPoints(ex, ctx) {

		Clothes.simpleStrokeFill(ctx, ex, this);

		const {
			ankleOut, 
			toeOut, 
			toeIn, 
			ankleIn,
			tipOut,
			tipIn,
			tongueOut,
			tongueIn,
			lacingPoints,
		} = calcSneakers.call(this, ex);
	
	
	//BASE
		ctx.beginPath();
		drawPoints(ctx, 
			ex.ankle.out,
			ankleOut, 
			toeOut, 
			toeIn, 
			ankleIn, 
			ex.ankle.in,
			
		);
		ctx.fill();
		ctx.stroke();
		
	//TIP	
		if(this.tipColor){
			ctx.fillStyle = this.tipColor;
		}else{
			ctx.fillStyle = this.highlight;
		}
		ctx.beginPath();
		drawPoints(ctx, 
			tipOut, 
			toeIn, 
			tipIn,
			tipOut,
		);
		ctx.fill();
		ctx.stroke();

	//TONGUE	
		ctx.fillStyle = ctx.strokeStyle;
		ctx.beginPath();
		drawPoints(ctx, 
			tongueIn,
			tongueOut,
			tongueIn
		);
		ctx.fill();
		ctx.stroke();
		
	//LACING
		ctx.lineWidth = this.laceThickness;
		ctx.strokeStyle = this.highlight;
		ctx.beginPath();
		drawPoints(ctx, 
			...lacingPoints.inner,
			breakPoint,
			...lacingPoints.outer,
		);
		ctx.stroke();

    }
}


export class LacedBootsPart extends HeelPart {
    constructor(...data) {
        super({},{
			tip: 0,
			tipColor: "#783b1a", 
			highlight: "#bb724a", 
			crosses: 0,
			laceThickness: 0.8,
			tongue: 2,
		},...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);

		const crosses = this.crosses ? this.crosses : (1-this.legCoverage) * 30;
		let {
			ankleOut, 
			toeOut, 
			toeIn, 
			ankleIn,
			tipOut,
			tipIn,
		} = calcShoeBase.call(this, ex);
		const {outerPoints, innerPoints} = calcSuperSocks.call(this,ex);
		
	//BASE
		ctx.beginPath();
		drawPoints(ctx, 
			...outerPoints,
			//ex.ankle.out,
			ankleOut, 
			toeOut, 
			toeIn, 
			ankleIn, 
			//ex.ankle.in,
			...innerPoints,
			extractPoint(outerPoints[0])
		);
		ctx.fill();
		ctx.stroke();
		
		
		
	//TIP	
		if(this.tip>-8){
			if(this.tipColor){
				ctx.fillStyle = this.tipColor;
			}else{
				ctx.fillStyle = this.highlight;
			}
			ctx.beginPath();
			drawPoints(ctx, 
				tipOut, 
				toeIn, 
				tipIn,
				tipOut,
			);
			ctx.fill();
			ctx.stroke();
		};
		
		
 	
	//LACING	
		const outMid = extractPoint(adjust(ex.ankle.out,-2,0));
		const outBot = adjust(outMid,-2,-9);
		const inMid = extractPoint(adjust(ex.ankle.in,2,0));
		const inBot = adjust(inMid,-0.5,-9)
		
		const width = (outBot.x - inBot.x);
		const center = findBetween(outerPoints[0],innerPoints[innerPoints.length-1]) 

		const outTop =   adjust(center,width,0) ;
		const inTop =  adjust(center,-width,0) ;

		//const points = getLacingPoints(outBot,outMid,inBot,inMid,this.crosses,0);
		const points = getLacingPoints(outBot,outMid,inBot,inMid,2,0);
		const points_2 = getLacingPoints(outMid,outTop,inMid,inTop,crosses,0);
		
		
	//TONGUE
		const tongueOut = adjust(outTop,-1,0);
		const tongueIn = adjust(inTop,1,0)
		tongueOut.cp1 = perpendicularPoint(outTop,tongueIn,0.5,-this.tongue);
		

	//TONGUE 2 
		ctx.fillStyle =	ctx.strokeStyle;
		ctx.beginPath();
		drawPoints(ctx, 
			tongueOut,
			
			outTop,
			outMid,
			outBot,
			
			inBot,
			inMid,
			inTop,
			
			tongueIn,
			tongueOut,
		);
		ctx.fill();
		
	//LACING 2 
		ctx.lineWidth = this.laceThickness;
		ctx.strokeStyle = this.highlight;
		ctx.beginPath();
		drawPoints(ctx, 
			...points.inner,
			breakPoint,
			...points.outer,
			breakPoint,
			...points_2.inner,
			breakPoint,
			...points_2.outer,
		);
		ctx.stroke();
		

    }
}



/* */


export class Sneakers extends Heels {
    constructor(...data) {
        super({
			tight: 0.5,
			shoeHeight: 0,
        }, ...data);
    }
	
	fill() {
        return "#114d91";
    }
	
	stroke() {
       return "#1a2a3c";
    }

    get partPrototypes() {
        return [
			{
                side: Part.LEFT,
                Part: SneakersPart
            },{
                side: Part.RIGHT,
                Part: SneakersPart
            },
            
		
 
        ];
    }
}


export class LacedBoots extends Heels {
    constructor(...data) {
        super({
			tight: 0.5,
			shoeHeight: 0,
			legCoverage: 0.7,
        }, ...data);
    }

	fill() {
        return "#bb724a";
    }
	
	stroke() {
       return "#783b1a";
    }
	
    get partPrototypes() {
        return [
		
			//BASE
            {
                side: Part.LEFT,
                Part: LacedBootsPart
            },{
                side: Part.RIGHT,
                Part: LacedBootsPart
            },
            
		
 
        ];
    }
}