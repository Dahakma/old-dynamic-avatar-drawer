import {FacePart} from "./face_part";
import {Part} from "../parts/part";
import {
    clamp,
    adjust,
    rotatePoints,
} from "drawpoint/dist-esm";

class Eyes extends FacePart {
    constructor(...data) {
        super({
            loc       : "eyes",
            aboveParts: ["parts head"],
        }, ...data);
    }
}


export class EyesHuman extends Eyes {
    constructor(...data) {
        super(...data);
    }

    fill() {
        return "white";
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            let h = this.faceLength * 0.1;

            const eyes = ex.eyes = {};
            eyes.center = {
                x: this.faceWidth * 0.04469 - mods.eyeCloseness * 0.01 + this.eyeSize * 0.005 +
                   mods.eyeWidth * 0.04,
                y: ex.skull.y - h * 0.42 + mods.eyeHeight * 0.1,
            };
            eyes.in = {
                x: eyes.center.x - 1.7 - this.eyeSize * 0.005 - mods.eyeWidth * 0.04,
                y: eyes.center.y - 0.5,
            };
            eyes.out = {
                x: eyes.in.x + 3.5 + this.eyeSize * 0.015 + mods.eyeWidth * 0.1,
                y: eyes.in.y + 0.3
            };

            const w = eyes.out.x - eyes.in.x;
            eyes.top = {
                x  : eyes.in.x + w * 0.4 + this.faceFem * 0.008,
                // x: eyes.in.x + w * 0.5 + mods.eyeBias * 0.1,
                y  : eyes.in.y + 1 + this.eyeSize * 0.015 + mods.eyeTopSize * 0.1,
                cp1: {
                    x: eyes.in.x + w * 0.1,
                    y: eyes.in.y + clamp(0.9 - this.faceFem * 0.015, 0.4, 2) +
                       this.eyeSize * 0.01
                }
            };
            eyes.top.cp2 = {
                x: eyes.top.x - w * 0.2 - mods.eyeTopSize * 0.05,
                y: eyes.top.y
            };
            eyes.top = adjust(eyes.top, mods.eyeBias * 0.1, 0);

            eyes.out.cp1 = {
                x: eyes.top.x + w * 0.2,
                y: eyes.top.y
            };
            eyes.out.cp2 = {
                x: eyes.out.x - w * 0.1 + this.eyeSize * 0.01,
                y: eyes.out.y + 0.5 + this.eyeSize * 0.01
            };

            // returning back to the center
            eyes.in.cp1 = {
                x: eyes.out.x - w * clamp(0.3 - mods.eyeBotBias * 0.03, 0, 1),
                // x: eyes.out.x - w * 0.3,
                y: eyes.out.y - 0.7 - this.eyeSize * 0.02 - mods.eyeBotSize * 0.1
            };
            eyes.in.cp2 = {
                x: eyes.in.x + w * clamp(0.2 + mods.eyeBotBias * 0.02, 0, 1),
                // x: eyes.in.x + w * 0.2,
                y: eyes.in.y
            };

            rotatePoints(eyes.center, mods.eyeTilt * Math.PI / 180, eyes.top,
                eyes.out,
                eyes.in);

        }

        return [
            ex.eyes.in,
            ex.eyes.top,
            ex.eyes.out, ex.eyes.in
        ];
    }
}

