/**
 * Core vitals (where user would define their gameplay stats that are constantly changing, e.g. hp, mp, stamina)
 * These describe the distribution of the maximum values
 * @memberof module:da
 */
export const vitalLimits = {
    stamina: {
        display: "STM", // abbreviation for rendering on canvas
        color: "hsl(80, 50%, 50%)", // color for rendering on canvas
        // description of distribution for randomly creating a character
        low  : 0,
        high : 500,
        avg  : 30,
        stdev: 6,
        bias : 0,
        // this calculates the derived max value
        calc() {
            // get properties with this.get methods
            return this.getBaseVitals('stamina');
        },
    },
    sanity: {
        display: "SAN",
        color: "hsl(30, 50%, 50%)",
        low  : 0,
        high : 100,
        avg  : 50,
        stdev: 6,
        bias : 0,
    },
};
