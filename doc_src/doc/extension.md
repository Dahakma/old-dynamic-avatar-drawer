---
layout: page
title: Extension guide
permalink: /extension.html
css: syntax
group: Extension
---
All extensions should be defined before `da.load().then(...)`.

## Add Gameplay Properties
To add stats, vitals, and Mods relevant for your game, define a property on the da.limit
objects, which provides a statistical description of that property.
You will want to connect the statistic you create to how physical dimensions are calculated,
which is detailed in the next section.

Take a look at `src/player/stats.js`, `src/player/vitals.js` and so on to see the format each
one should take.

```javascript
// create additional core stat of int (intelligence) with default value of 5
// give the stat some bounds and distribution
da.statLimits["int"] = {
    low: 0,
    high: 10,
    avg: 5,
    stdev: 2.5,
    bias: 0.3
};

// define maximum values for constantly changing values (such as HP) 
da.vitalLimits["sanity"] = {
    display: "SAN", // abbreviation for rendering on canvas
    color: "hsl(30, 50%, 50%)", // color for rendering on canvas
    low: 0,
    high: 20,
    avg: 8,
    stdev: 2,
    bias: 0,
    calc() {
        // higher intelligence reduces maximum sanity - easier to go crazy when you're smarter...
        return this.getBaseVitals('sanity') - this.get('int') * 0.5;
    }
};

// you can do the same with modifiers
// as soon as you create a new stat, a new modifier will automatically be created
da.modLimits["tanned"] = {
    low:0,
    high:1e9,
    avg:1,
    stdev:2,
    bias:0
};
```

### Linking Game Stats to Drawing
Now that you defined properties for your own game, you might want them to affect how the avatar is drawn.
You must link your game properties to how specific physical dimensions are calculated.

```javascript
// the first argument is a string of {skeleton}.{dimension}
// base is the dimension value calculated from all functions before
da.extendDimensionCalc("human.handSize", function (base) {
    return base + this.get("int") * 2;
});
```

These methods have the avatar as the `this` argument,
which means you can call functions like `this.getDim(...)` and `this.get(...)`. 
See `src/player/dimensions.js` for some functions you might want to use in your calculations.


## Extending Player
For more complicated extensions, you'll want to subclass the `Player` class

```javascript
class MyPlayer extends da.Player {
    constructor(data) {
        const allData = Object.assign({
            friends: [],// track who's their friend
            todo: [],   // action stack to complete
        }, data);
        super(allData);
        // do more initialization
    }
    // define other methods
}
```