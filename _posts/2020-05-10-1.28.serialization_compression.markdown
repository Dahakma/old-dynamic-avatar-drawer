---
layout: post
title:  "1.28 Serialization Compression"
date:   2020-05-10
categories: release
---

- updated serialization
    - save only properties that are different from default
    - remove circular references (serialization/deserialization should be compatible with SugarCube 2 now)
- include transformation completion percentage to transform function
    - enables more advanced transformations
    
    
## Transformation
Since expressions need to be applied via the `<Player>.applyExpression` method,
we can't directly use expression as the modifying object to the player object as
that would permanently transform the facial expression. Instead, we can use
the transformation framework to smoothly update the expression.

Thus the transformation framework can be thought of more generally as
a way to smoothly update something, rather than just to permanently change
property values. 
```javascript
var _transform = da.createTransformation({}, function(completion) {
	$PC.applyExpression(da.Expression.create(da.Expression[_expression], completion * _expressionDegree));
	drawToMenu($PC);
}, {});
da.transformAndShow(_transform, _transformDuration);
```
